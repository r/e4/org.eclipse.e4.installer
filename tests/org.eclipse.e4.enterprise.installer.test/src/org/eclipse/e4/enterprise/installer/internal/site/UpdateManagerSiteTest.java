/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.internal.site;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.enterprise.installer.InstallError;
import org.eclipse.e4.enterprise.installer.internal.site.InstallationSiteManager;
import org.eclipse.e4.enterprise.installer.internal.site.UpdateManagerSite;
import org.eclipse.e4.enterprise.installer.internal.site.UpdateManagerSite.UpdateSiteFeatureXML;
import org.eclipse.e4.ui.test.utils.FixturesLocation;
import org.eclipse.e4.ui.test.utils.TestCaseX;
import org.eclipse.update.core.IFeature;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.ISite;
import org.eclipse.update.core.ISiteFeatureReference;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.VersionedIdentifier;



public class UpdateManagerSiteTest extends TestCaseX {
	private UpdateManagerSite testee;
	private File downloadRootDir;

	protected void setUp() throws Exception {
		downloadRootDir = new File(System.getProperty("java.io.tmpdir") + "/obtest");
		testee = (UpdateManagerSite) new InstallationSiteManager(downloadRootDir).create();
	}

	// downloadToUpdateSet--------------------------------------------------------------------------------------------	
	private static final String SITE400_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.0/site.xml";

	public void testConvertIntoUpdateSite_CheckThatUpdateSiteContainsSameFeaturesAsConfigSiteItIsGeneratedFrom() throws Exception {
		UpdateManagerSite testee = createTemporaryDownloadSiteWithDownloadedContents();
		try {
			testee.convertIntoUpdateSite();
			assertUpdateSiteHasIdenticalFeaturesAsDownloadSite(testee);
		} finally {
			testee.removeFromConfiguredSites();  // Make sure we don't leave Eclipse in a wierd state
		}
	}

	private void assertUpdateSiteHasIdenticalFeaturesAsDownloadSite(
			UpdateManagerSite testee) throws CoreException, InstallError {
		URL url = testee.getConfiguredSite().getSite().getURL();
		IFeatureReference[] featureReferences = SiteManager.getSite(url, false, new NullProgressMonitor()).getFeatureReferences();
		
		List<IFeatureReference> featureReferencesList = Arrays.asList(featureReferences);
		
		assertTrue("Update site should have identical Features as download site", 
				testee.hasIdenticalFeatures(featureReferencesList));
	}

	private UpdateManagerSite createTemporaryDownloadSiteWithDownloadedContents() throws IOException,
			CoreException, MalformedURLException, InvocationTargetException 
	{
		File tempDir = createTempDir("1BenchTest-");	// start w/ numeral so that it sorts to the top
		System.out.println("Temp update site dir = " + tempDir.getAbsolutePath()); // for debugging purposes
		
		InstallationSiteManager siteBuilder = new InstallationSiteManager(tempDir);
		UpdateManagerSite temporaryDownloadSite = (UpdateManagerSite) siteBuilder.create();
		temporaryDownloadSite.addToConfiguredSites();
		
		ISiteFeatureReference[] remoteFeatures = SiteManager.getSite(
				new URL(SITE400_FIXTURE), false, new NullProgressMonitor())
					.getFeatureReferences();
		temporaryDownloadSite.install(remoteFeatures, new NullProgressMonitor());
		
		return temporaryDownloadSite;
	}
	
	// computeUpdateSiteFeatureData--------------------------------------------------------------------------------

	public void testComputeUpdateSiteFeatureData_emptyInput_resultsInEmptyOutput() throws Exception {
		UpdateSiteFeatureXML[] result = testee.computeUpdateSiteFeatureData(
				new IFeatureReference[] {});
		assertEquals(0, result.length);
	}
	
	public void testComputeUpdateSiteFeatureData_indexesMatch() throws Exception {
		IFeatureReference[] input = new IFeatureReference[] {
				newFeatureRef("file:///c:/path/to/features/featurefolder/",
						"com.foo.feature.id", "1.0.0"),
				newFeatureRef("file:///c:/path/to/features/featurefolder/",
						"com.foo.feature.id", "1.0.0"),
		};

		UpdateSiteFeatureXML[] result = testee.computeUpdateSiteFeatureData(input);
		
		assertEquals(2, result.length);
	}
	
	// computeFeatureXMLData---------------------------------------------------------------------------------------

	public void testComputeFeatureXMLData_dataMatches() throws Exception {
		IFeatureReference input = newFeatureRef(
				"file:///c:/path/to/features/featurefolder/",
				"com.foo.feature.id", "1.0.0");

		UpdateSiteFeatureXML result = testee.computeFeatureXMLData(input);
		
		assertEquals("features/featurefolder.jar", result.jarRelativePath);
		assertEquals("com.foo.feature.id", result.id);
		assertEquals("1.0.0", result.version);
	}
	
	// areFeaturesDifferent?---------------------------------------------------------------------------------------
	
	public void testAreFeaturesDifferent_catchesListsInDifferentOrders(){
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new LinkedList<String>();
		
		list1.add("thing 1");
		list1.add("thing 2");
		list1.add("thing 3");

		list2.add("thing 3");
		list2.add("thing 1");
		list2.add("thing 2");
		
		assertFalse(testee.areFeaturesDifferent(list1, list2));
	}		
	
	public void testAreFeaturesDifferent_matchesSimpleDuplicate(){
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new LinkedList<String>();
		
		list1.add("the same thing");
		list2.add("the same thing");
		
		assertFalse(testee.areFeaturesDifferent(list1, list2));
	}		

	public void testAreFeaturesDifferent_catchesDifferentNoEmptyLists(){
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new LinkedList<String>();
		
		list1.add("something");
		list1.add("somethingelse");
		
		assertTrue(testee.areFeaturesDifferent(list1, list2));
	}		

	public void testAreFeaturesDifferent_catchesDifferentListAndEmptyList(){
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new LinkedList<String>();
		
		list1.add("something");
		
		assertTrue(testee.areFeaturesDifferent(list1, list2));
	}		
	
	public void testAreFeaturesDifferent_matchesEmptyLists(){
		List<String> empty1 = new ArrayList<String>();
		List<String> empty2 = new LinkedList<String>();
		
		assertFalse(testee.areFeaturesDifferent(empty1, empty2));
	}

	//--------------------------------------------------------------------------------
	
	private IFeatureReference newFeatureRef(final String url, final String id, final String version) {
		return new IFeatureReference() {

			public IFeature getFeature() throws CoreException {
				return null;
			}

			public IFeature getFeature(IProgressMonitor monitor)
					throws CoreException {
				return null;
			}

			public String getName() {
				return null;
			}

			public ISite getSite() {
				return null;
			}

			public URL getURL() {
				URL result = null;
				try {
					result = new URL(url);
				} catch (MalformedURLException e) {
					fail();
				}
				return result;
			}

			public VersionedIdentifier getVersionedIdentifier()
					throws CoreException {
				return new VersionedIdentifier(id, version);
			}

			public boolean isPatch() {
				return false;
			}

			public void setSite(ISite site) {
			}

			public void setURL(URL url) throws CoreException {
			}

			@SuppressWarnings("unchecked")
			public Object getAdapter(Class adapter) {
				return null;
			}

			public String getNL() {
				return null;
			}

			public String getOS() {
				return null;
			}

			public String getOSArch() {
				return null;
			}

			public String getWS() {
				return null;
			}};
	}


}
