/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.enterprise.installer.internal.FeatureReferenceTree;
import org.eclipse.e4.enterprise.installer.internal.site.InstallationSite;
import org.eclipse.e4.enterprise.installer.internal.site.InstallationSiteManager;
import org.eclipse.e4.enterprise.installer.internal.site.StubInstallationSite;
import org.eclipse.e4.ui.test.utils.FixturesLocation;
import org.eclipse.e4.ui.test.utils.TestCaseX;
import org.eclipse.update.core.IFeature;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.ISite;
import org.eclipse.update.core.VersionedIdentifier;


public class BundleUpdaterTest extends TestCaseX {
	private static final String FIXTURE_FEATURE_ID = "org.eclipse.e4.enterprise.installer.test.fixture.feature";

	private static final String FIXTURE_FEATURE_ID_A = "org.eclipse.e4.enterprise.installer.test.fixture.many_a.feature";
	private static final String FIXTURE_FEATURE_ID_B = "org.eclipse.e4.enterprise.installer.test.fixture.many_b.feature";
	private static final String FIXTURE_FEATURE_ID_C = "org.eclipse.e4.enterprise.installer.test.fixture.many_c.feature";

	private static final String SITE400_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.0/site.xml";
	private static final String SITEABC_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_ABC/site.xml";
	private static final String SITE401_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.1/site.xml";
	private static final String SITE500_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_5.0.0/site.xml";
	private static final String FOO_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/no_site_ABC/foo.xml";

	private URL SITE400_URL;
	private URL SITEABC_URL;
	private URL SITE401_URL;
	private URL SITE500_URL;
	private URL FOO_URL;


	BundleUpdater testee;

	public BundleUpdaterTest() throws MalformedURLException {
		SITE400_URL = new URL(SITE400_FIXTURE);
		SITEABC_URL = new URL(SITEABC_FIXTURE);
		SITE401_URL = new URL(SITE401_FIXTURE);
		SITE500_URL = new URL(SITE500_FIXTURE);
		FOO_URL = new URL(FOO_FIXTURE);

	}

	protected void setUp() throws Exception {
		super.setUp();
		testee = new BundleUpdater(null);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}


	// Custom-Asserts--------------------------------------------------------------------------------------------------------------------

	private void assertFeatureFoundIn(List<IFeatureReference> list, String featureID) throws Exception {
		for (IFeatureReference feature : list) {
			if (feature.getVersionedIdentifier().getIdentifier().equals(featureID)) {
				return;
			}
		}
		fail("Expected pruned feature to install list to contain: " + featureID);
	}

	// calculateFeaturesReferencesToInstall-----------------------------------------------------------------------------------------------

	public void testVersionQualifiersAreOrderedAsAStringComparisonWhenUsingList() throws InstallError {		
		List<IFeatureReference> featuresToLookIn = new ArrayList<IFeatureReference>();
		Set<FeatureVersionedIdentifier> featuresToProvision = new HashSet<FeatureVersionedIdentifier>();
		
		String featureID = "MyFeature";
		featuresToLookIn.add(FeatureReferenceMother.createFeatureReferenceFromFVIWithNoChildren(
				new FeatureVersionedIdentifier(featureID, "1.0.0.M7")));
		IFeatureReference expectedIFR = FeatureReferenceMother.createFeatureReferenceFromFVIWithNoChildren(
				new FeatureVersionedIdentifier(featureID, "1.0.0.R3"));
		featuresToLookIn.add(expectedIFR);
		
		featuresToProvision.add(new FeatureVersionedIdentifier(featureID, "0.0.0"));
		
		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresToLookIn, featuresToProvision, true);
		
		assertEquals(1, result.size());
		assertTrue(result.contains(expectedIFR));
	}
	
	public void testVersionQualifiersAreOrderedAsAStringComparisonWhenUsingSet() throws InstallError {		
		FeatureReferenceTree tree = new FeatureReferenceTree();
		String featureID = "MyFeature";
		FeatureVersionedIdentifier oldFvi = new FeatureVersionedIdentifier(featureID, "1.0.0.M7");
		IFeatureReference oldFeature = FeatureReferenceMother.createFeatureReferenceFromFVIWithNoChildren(oldFvi);
				
		FeatureVersionedIdentifier newFvi = new FeatureVersionedIdentifier(featureID, "1.0.0.R3");
		IFeatureReference newFeature = FeatureReferenceMother.createFeatureReferenceFromFVIWithNoChildren(newFvi);
		
		tree.add(newFeature);
		tree.add(oldFeature);
				
		Set<FeatureVersionedIdentifier> setToProvision = new HashSet<FeatureVersionedIdentifier>();
		setToProvision.add(new FeatureVersionedIdentifier(featureID, "0.0.0"));
		
		Set<FeatureVersionedIdentifier> result = testee.resolveVersionNumbersAsFVISet(tree, setToProvision, true);
		
		assertEquals(1, result.size());
		assertTrue(result.contains(newFvi));	
	}
	
	// 0 FRs come from local, the rest from remote
	public void testCalculateFeaturesReferencesToInstall_0FeatureRefsComeFromTheLocalSite() throws Exception {
		List<IFeatureReference> local = new ArrayList<IFeatureReference>();
		List<IFeatureReference> remote = new ArrayList<IFeatureReference>();
		remote.add(new SpecialStubFeatureReference("f1", false));
		remote.add(new SpecialStubFeatureReference("f2", false));
		remote.add(new SpecialStubFeatureReference("f3", false));

		List<IFeatureReference> result = testee.calculateFeaturesReferencesToInstall(remote, local);

		for (IFeatureReference featureReference : result) {
			SpecialStubFeatureReference stubFR = (SpecialStubFeatureReference) featureReference;
			assertFalse("featureName:" + stubFR.name, stubFR.isLocal);
		}
	}

	// 1 FR comes from local, the rest from remote
	public void testCalculateFeaturesReferencesToInstall_1FeatureRefComesFromTheLocalSite() throws Exception {
		String featureOnBothLocalAndRemote = "f2";
		List<IFeatureReference> local = new ArrayList<IFeatureReference>();
		local.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote, true));
		List<IFeatureReference> remote = new ArrayList<IFeatureReference>(); 
		remote.add(new SpecialStubFeatureReference("f1", false));
		remote.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote, false));
		remote.add(new SpecialStubFeatureReference("f3", false));

		List<IFeatureReference> result = testee.calculateFeaturesReferencesToInstall(remote, local);

		for (IFeatureReference featureReference : result) {
			SpecialStubFeatureReference stubFR = (SpecialStubFeatureReference) featureReference;
			String name = stubFR.name;
			if (featureOnBothLocalAndRemote.equals(name)) {
				assertTrue("featureName:" + name, stubFR.isLocal);
			} else {
				assertFalse("featureName:" + name, stubFR.isLocal);
			}
		}
	}

	// n FRs come from local, the rest from remote
	public void testCalculateFeaturesReferencesToInstall_ManyFeatureRefComesFromTheLocalSite() throws Exception {
		String featureOnBothLocalAndRemote1 = "f2";
		String featureOnBothLocalAndRemote2 = "f3";
		
		HashSet<String> localNames = new HashSet<String>(2);
		localNames.add(featureOnBothLocalAndRemote1);
		localNames.add(featureOnBothLocalAndRemote2);
		
		List<IFeatureReference> local = new ArrayList<IFeatureReference>();
		local.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote1, true));
		local.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote2, true));
		
		List<IFeatureReference> remote = new ArrayList<IFeatureReference>();
		remote.add(new SpecialStubFeatureReference("f1", false));
		remote.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote1, false));
		remote.add(new SpecialStubFeatureReference(featureOnBothLocalAndRemote2, false));
		remote.add(new SpecialStubFeatureReference("f4", false));
		
		List<IFeatureReference> result = testee.calculateFeaturesReferencesToInstall(remote, local);

		for (IFeatureReference featureReference : result) {
			SpecialStubFeatureReference stubFR = (SpecialStubFeatureReference) featureReference;
			String name = stubFR.name;
			if (localNames.contains(name)) {
				assertTrue("featureName:" + name, stubFR.isLocal);
			} else {
				assertFalse("featureName:" + name, stubFR.isLocal);
			}
		}
	}
	// all come from local.
	public void testCalculateFeaturesReferencesToInstall_AllFeatureRefsComeFromTheLocalSite() throws Exception {
		List<IFeatureReference> local = new ArrayList<IFeatureReference>(); 
		local.add(new SpecialStubFeatureReference("f1", true));
		local.add(new SpecialStubFeatureReference("f2", true));
		local.add(new SpecialStubFeatureReference("f3", true));
		List<IFeatureReference> remote = new ArrayList<IFeatureReference>();

		List<IFeatureReference> result = testee.calculateFeaturesReferencesToInstall(remote, local);

		for (IFeatureReference featureReference : result) {
			SpecialStubFeatureReference stubFR = (SpecialStubFeatureReference) featureReference;
			assertTrue("featureName:" + stubFR.name, stubFR.isLocal);
		}
	}
	// setFeaturesToProvisionAsSetOfAllFeaturesOnRemoteSitesIfUnspecified-----------------------------------------------------------------

	public void testSetFeaturesToProvisionAsSetOfAllFeaturesOnRemoteSitesIfUnspecified_whenProvisingSetNull_CopiesFromIFRs() throws Exception {
		FeatureReferenceTree input = testee.getAllFeaturesFromUpdateSites(new URL[] { SITEABC_URL });

		Set<FeatureVersionedIdentifier> result = testee.setFeaturesToProvisionToBeOfAllFeaturesOnRemoteSitesIfUnspecified(null, input);

		assertEquals(3, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_A, "1.0.0")));
		assertTrue(result.contains(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_B, "1.0.0")));
		assertTrue(result.contains(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_C, "1.0.0")));
	}

	public void testSetFeaturesToProvisionAsSetOfAllFeaturesOnRemoteSitesIfUnspecified_returnsFirstArgWhenFirstArgNonNull() throws Exception {
		Set<FeatureVersionedIdentifier> input = new HashSet<FeatureVersionedIdentifier>();
		input.add(new FeatureVersionedIdentifier("oink", "1.2.3"));
		input.add(new FeatureVersionedIdentifier("moo", "2.3.4"));
		input.add(new FeatureVersionedIdentifier("baah", "3.4.5"));

		Set<FeatureVersionedIdentifier> result = testee.setFeaturesToProvisionToBeOfAllFeaturesOnRemoteSitesIfUnspecified(input, new FeatureReferenceTree());

		assertTrue(input.equals(result));
	}


	// extractCorrectVersionsOfFeatureReferenceToProvision------------------------------------------------------------------------------------

	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_whereFeatureToBeProvisionedHasVersion000_updateSitesHaveMultipleVersions_getsLatestVersions_andIsNotOrderDependent() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID, "0.0.0"));
		
		FeatureVersionedIdentifier expectedFVI = new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID, "4.0.1");
		
		// Ordered 400, 401
		FeatureReferenceTree featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITE400_URL, SITE401_URL });
		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);
		assertEquals(1, result.size());
		assertEquals(expectedFVI, new FeatureVersionedIdentifier(result.get(0)));
		
		// Ordered 401, 400 - this was causing an error
		featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITE401_URL, SITE400_URL });
		result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);
		assertEquals(1, result.size());
		assertEquals(expectedFVI, new FeatureVersionedIdentifier(result.get(0)));
	}
	
	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_featureToProvisionVersion000_UpdateSiteFeatureVersionNot000() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID, "0.0.0"));
		
		FeatureReferenceTree featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITE400_URL });

		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);

		assertEquals(1, result.size());
		assertEquals(featuresOnUpdateSite.getAllReferences().get(0), result.get(0));
	}

	
	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_FeatureNotAvailableThrowsException() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier("a_feature_that_doesnt_exist on_the_site", "1.0.0"));

		try {
			testee.resolveVersionNumbersAsIFRList(new ArrayList<IFeatureReference>(), featureIDsToProvision, true);
			fail("Should have failed");
		} catch (InstallError e) {
			// success
		}
	}

	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_FeatureNotAvailableStillSucceeds() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier("a_feature_that_doesnt_exist on_the_site", "1.0.0"));

		try {
			testee.resolveVersionNumbersAsIFRList(new ArrayList<IFeatureReference>(), featureIDsToProvision, false);
			// success
		} catch (InstallError e) {
			fail("Should not have failed");
		}
	}

	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_featureToProvisionSameAsUpdateSiteFeatures_oneFeature() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID, "4.0.0"));
		FeatureReferenceTree featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITE400_URL });

		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);

		assertEquals(1, result.size());
		assertEquals(featuresOnUpdateSite.getAllReferences().get(0), result.get(0));
	}

	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_featureToProvisionSameAsUpdateSiteFeatures_threeFeatures() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_A, "1.0.0"));
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_B, "1.0.0"));
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_C, "1.0.0"));
		FeatureReferenceTree featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITEABC_URL });

		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);

		assertEquals(3, result.size());
		
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_A);
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_B);
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_C);				
	}

	// test where provisioning list is a subset of features on site
	public void testExtractCorrectVersionsOfFeatureReferenceToProvision_featureToProvisionFewerThanUpdateSiteFeatures() throws Exception {
		Set<FeatureVersionedIdentifier> featureIDsToProvision = new HashSet<FeatureVersionedIdentifier>();
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_A, "1.0.0"));
		featureIDsToProvision.add(new FeatureVersionedIdentifier(FIXTURE_FEATURE_ID_C, "1.0.0"));
		FeatureReferenceTree featuresOnUpdateSite = testee.getAllFeaturesFromUpdateSites(new URL[] { SITEABC_URL });

		List<IFeatureReference> result = testee.resolveVersionNumbersAsIFRList(featuresOnUpdateSite.getAllReferences(), featureIDsToProvision, true);

		assertEquals(2, result.size());
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_A);
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_C);
	}

	// getAllFeaturesFromUpdateSites-----------------------------------------------------------------------------------------------------

	public void testGetAllFeaturesFromUpdateSite_AlternativeNameXML_returnsAllFeatures() throws Exception {
		URL[] updateSites = new URL[] { FOO_URL};

		List<IFeatureReference> result = testee.getAllFeaturesFromUpdateSites(updateSites).getAllReferences();
		
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_A);
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_B);
		assertFeatureFoundIn(result, FIXTURE_FEATURE_ID_C);			
	}
	
	public void testGetAllFeaturesFromUpdateSites_worksFor2Sites() throws Exception {
		URL[] updateSites = new URL[] { SITE400_URL, SITE401_URL };

		FeatureReferenceTree allFeaturesFromUpdateSites = testee.getAllFeaturesFromUpdateSites(updateSites);

		assertEquals(2, allFeaturesFromUpdateSites.getAllReferences().size());

		String[] resultVersions = extractVersionStringsAndSort(allFeaturesFromUpdateSites.getAllReferences());

		assertEquals("org.eclipse.e4.enterprise.installer.test.fixture.feature_4.0.0", resultVersions[0]);
		assertEquals("org.eclipse.e4.enterprise.installer.test.fixture.feature_4.0.1", resultVersions[1]);
	}

	private String[] extractVersionStringsAndSort(List<IFeatureReference> allFeaturesFromUpdateSites) throws CoreException {
		String[] resultVersions = new String[allFeaturesFromUpdateSites.size()];
		for (int i = 0; i < allFeaturesFromUpdateSites.size(); i++) {
			resultVersions[i] = allFeaturesFromUpdateSites.get(i).getVersionedIdentifier().toString();
		}
		Arrays.sort(resultVersions);
		return resultVersions;
	}

	public void testGetAllFeaturesFromUpdateSites_worksForSingleSite() throws Exception {
		URL[] updateSites = new URL[] { SITE400_URL };

		FeatureReferenceTree allFeaturesFromUpdateSites = testee.getAllFeaturesFromUpdateSites(updateSites);

		assertEquals(1, allFeaturesFromUpdateSites.getAllReferences().size());

		IFeatureReference featureReference = allFeaturesFromUpdateSites.getAllReferences().get(0);
		assertEquals("org.eclipse.e4.enterprise.installer.test.fixture.feature_4.0.0", featureReference.getVersionedIdentifier().toString());
	}

	public static class StuntSiteManager extends InstallationSiteManager {
		public StuntSiteManager(File downloadRootDir) {
			super(downloadRootDir);
			downloadRootDir.delete();
			downloadRootDir.mkdirs();
		}

		int callCount = 0;

		public int getCallCount() {
			return callCount;
		}

		@Override
		public void restartInitiated() throws IOException {
			callCount++;
		}
	};

	public void testInstallAndRollBackOnError_WhenNoRestartIsNeeded_RestartInitiatedNotInvoked() throws Exception {
		InstallationSite currentSite = new StubInstallationSite(false);
		File downloadRoot = File.createTempFile("any", "thing");
		final StuntSiteManager stuntManager = new StuntSiteManager(downloadRoot);
		
		testee.installAndRollBackOnError(stuntManager, currentSite, new ArrayList<IFeatureReference>());

		assertEquals(0, stuntManager.getCallCount());
	}

	public void testIOExceptionInRestartManager_causesInstallRollbackAndInstallError() throws Exception {
		final StubInstallationSite newSite = new StubInstallationSite(true);
		StubInstallationSite currentSite = new StubInstallationSite(true);

		// stubs
		InstallationSiteManager stuntManager = new InstallationSiteManager(File.createTempFile("any", "thing")) {
			@Override
			public InstallationSite create() throws CoreException {
				return newSite;
			}
			@Override
			public void restartInitiated() throws IOException {
				throw new IOException();
			}
		};	

		try {
			testee.installAndRollBackOnError(stuntManager, currentSite, new ArrayList<IFeatureReference>());
			fail();
		} catch (InstallError e) {
			// want to be here
		}
		
		/*
		 * the following expectations embody "rollback". There may be better ways to
		 * represent "rolling back" as currently we are saying the same thing in
		 * two places (code, test). Perhaps this could be done with a proper
		 * transaction of some kind.
		 * 
		 * Really, what you want above is to know that
		 * "roll back has been initiated"; you probably don't need to care how
		 * the rollback works.
		 */
		assertEquals(1, newSite.removeFromConfiguredSitesCount);
		assertEquals(1, currentSite.addToConfiguredSitesCount);
	}
	
	// ------------------------------------------------------------------------------------
	
	public void testResolveVersionNumbersAsVFISet_cannotFindFeatureReference_featuresMustBePresent_throwsInstallError() throws Exception {
		FeatureReferenceTree tree = new FeatureReferenceTree();
		Set<FeatureVersionedIdentifier> toProvision = new HashSet<FeatureVersionedIdentifier>();
		toProvision.add(new FeatureVersionedIdentifier("org.eclipse.foo.bar", "1.1.0"));
		toProvision.add(new FeatureVersionedIdentifier("org.eclipse.foo2.bar", "1.1.0"));
		
		try {
			testee.resolveVersionNumbersAsFVISet(tree, toProvision, true);
			fail("Should have thrown an InstallError");
		} catch (InstallError e) {
			//success
		}
	}

	public void testResolveVersionNumbersAsVFISet_cannotFindFeatureReference_returnsEmptySet() throws Exception {
		FeatureReferenceTree tree = new FeatureReferenceTree();
		Set<FeatureVersionedIdentifier> toProvision = new HashSet<FeatureVersionedIdentifier>();
		toProvision.add(new FeatureVersionedIdentifier("org.eclipse.foo.bar", "1.1.0"));
		toProvision.add(new FeatureVersionedIdentifier("org.eclipse.foo2.bar", "1.1.0"));
		
		Set<FeatureVersionedIdentifier> result = testee.resolveVersionNumbersAsFVISet(tree, toProvision, false);
		assertTrue("returned set should be empty", result.isEmpty());
	}

	// ------------------------------------------------------------------------------------

	private class SpecialStubFeatureReference implements IFeatureReference {
		
//		This class smells fishy, there is another StubFeatureReference class that should
//		be used in this test class. The getOuterType() method is added automatically 
//		by the IDE, probably because this is in an inner class.
		
		public String name = "";
		public boolean isLocal = false;

		public SpecialStubFeatureReference(String name) {
			this.name = name;
		}

		public SpecialStubFeatureReference(String name, boolean isLocal) {
			this.name = name;
			this.isLocal = isLocal;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SpecialStubFeatureReference other = (SpecialStubFeatureReference) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		public IFeature getFeature() throws CoreException {
			// TODO Auto-generated method stub
			return null;
		}

		public IFeature getFeature(IProgressMonitor monitor) throws CoreException {
			// TODO Auto-generated method stub
			return null;
		}

		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}

		public ISite getSite() {
			// TODO Auto-generated method stub
			return null;
		}

		public URL getURL() {
			// TODO Auto-generated method stub
			return null;
		}

		public VersionedIdentifier getVersionedIdentifier() throws CoreException {
			// TODO Auto-generated method stub
			return null;
		}

		public boolean isPatch() {
			// TODO Auto-generated method stub
			return false;
		}

		public void setSite(ISite site) {
			// TODO Auto-generated method stub

		}

		public void setURL(URL url) throws CoreException {
			// TODO Auto-generated method stub

		}

		public Object getAdapter(Class adapter) {
			// TODO Auto-generated method stub
			return null;
		}

		public String getNL() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getOS() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getOSArch() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getWS() {
			// TODO Auto-generated method stub
			return null;
		}

		private BundleUpdaterTest getOuterType() {
			return BundleUpdaterTest.this;
		}

	}
}
