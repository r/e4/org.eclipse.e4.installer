/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import org.eclipse.e4.enterprise.installer.FeatureVersionedIdentifier;

import junit.framework.TestCase;

public class FeatureVersionedIdentifierTest extends TestCase {

	public void testConstructorValidatesVersionCorrectly() throws Exception {

		//valid:
		new FeatureVersionedIdentifier("name","1.2.3");
		
		//invalid:
		try{
			FeatureVersionedIdentifier testee = new FeatureVersionedIdentifier("name","1.monkey.foo");
			fail();
		}
		catch (IllegalArgumentException e) {
			// want to be here
		}
	}
	
}
