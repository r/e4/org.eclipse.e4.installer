/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.internal.site;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.update.core.IFeatureReference;

public class StubInstallationSite extends NullInstallationSite {
	
	private final boolean installReturnValue;
	public int addToConfiguredSitesCount = 0;
	public int removeFromConfiguredSitesCount = 0;

	public StubInstallationSite(boolean installReturnValue) {
		this.installReturnValue = installReturnValue;
		
	}
	
	@Override
	public boolean install(IFeatureReference[] updateSiteFeatures, IProgressMonitor progressMonitor) throws CoreException, InvocationTargetException {
		return installReturnValue;
	}

	@Override
	public void addToConfiguredSites() {
		addToConfiguredSitesCount++;
	}
	
	@Override
	public void removeFromConfiguredSites() {
		removeFromConfiguredSitesCount++;
	}
	
	

	
	
}
