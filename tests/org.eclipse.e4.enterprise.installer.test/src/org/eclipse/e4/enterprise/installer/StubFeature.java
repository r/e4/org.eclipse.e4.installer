/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.update.core.Feature;
import org.eclipse.update.core.IIncludedFeatureReference;

public class StubFeature extends Feature {

	private final IIncludedFeatureReference[] includedFeatureReferences;

	public StubFeature(IIncludedFeatureReference[] includedFeatureReferences) {
		this.includedFeatureReferences = includedFeatureReferences;	
	}
	

	@Override
	public IIncludedFeatureReference[] getIncludedFeatureReferences()
			throws CoreException {
		return includedFeatureReferences;
	}
	
}
