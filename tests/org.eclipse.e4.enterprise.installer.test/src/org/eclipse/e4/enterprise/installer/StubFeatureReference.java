/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.update.core.IFeature;
import org.eclipse.update.core.IncludedFeatureReference;
import org.eclipse.update.core.VersionedIdentifier;

public class StubFeatureReference extends IncludedFeatureReference {
	
	private IFeature feature;
	private VersionedIdentifier versionedIdentifier;
	
	public StubFeatureReference(IFeature feature){
		this.feature = feature;
	}
	
	public void setVersionedIdentifier(String id, String version){
		versionedIdentifier = new VersionedIdentifier(id, version);
	}
	
	@Override
	public VersionedIdentifier getVersionedIdentifier() {
		return versionedIdentifier;
	}
	
	@Override
	public IFeature getFeature() throws CoreException {
		return feature;
	}

	@Override
	public IFeature getFeature(IProgressMonitor monitor) throws CoreException {
		return feature;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((feature == null) ? 0 : feature.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StubFeatureReference other = (StubFeatureReference) obj;
		if (feature == null) {
			if (other.feature != null)
				return false;
		} else if (!feature.equals(other.feature))
			return false;
		return true;
	}

	
	
}
