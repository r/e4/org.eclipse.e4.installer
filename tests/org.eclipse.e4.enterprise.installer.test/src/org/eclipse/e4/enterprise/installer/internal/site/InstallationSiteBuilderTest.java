/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.internal.site;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import org.eclipse.e4.enterprise.installer.internal.site.InstallationSite;
import org.eclipse.e4.enterprise.installer.internal.site.InstallationSiteManager;
import org.eclipse.e4.enterprise.installer.internal.site.NullInstallationSite;
import org.eclipse.e4.enterprise.installer.internal.site.UpdateManagerSite;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.core.SiteManager;


public class InstallationSiteBuilderTest extends TestCase {

	private File downloadRootDir;
	private InstallationSiteManager testee;

	protected void setUp() throws Exception {
		downloadRootDir = new File(System.getProperty("java.io.tmpdir") + "/obtest");
		downloadRootDir.mkdir();
		testee = new InstallationSiteManager(downloadRootDir);	
	}
	
	protected void tearDown() throws Exception {
		deleteDir(downloadRootDir);
	}
	
	public void testDirectoryNameFiltering() throws Exception {
		File root = File.createTempFile("downloadroot", "temp");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(InstallationSiteManager.CONFIG_SITE_DIR_DATE_NAME_FORMAT);
		Date now = new Date();
		String date = simpleDateFormat.format(now);

		long nowLong = now.getTime();

		File pruneMe1 = new File(root, "I should not be deleted");
		File pruneMe2 = new File(root, "_I should not be deleted");
		File pruneMe3 = new File(root, date + "something");
		File pruneMe4 = new File(root, date.substring(0, 10));
		

		File expected1 = new File(root, simpleDateFormat.format(now));
		File expected2 = new File(root, simpleDateFormat.format(new Date(nowLong + 12345)));
		File expected3 = new File(root, simpleDateFormat.format(new Date(nowLong + 123456)));
		File expected4 = new File(root, simpleDateFormat.format(new Date(nowLong + 1234567)));

		File[] testData = new File[] { expected4, pruneMe1, expected3, pruneMe2, expected2, pruneMe3, expected1 , pruneMe4};

		File[] result = testee.filterOutFilesThatDoNotMatchExpectedFormat(testData);
		
		assertTrue(arrayContains(result, expected1));
		assertTrue(arrayContains(result, expected2));
		assertTrue(arrayContains(result, expected3));
		assertTrue(arrayContains(result, expected4));
		
		assertFalse(arrayContains(result, pruneMe1));
		assertFalse(arrayContains(result, pruneMe2));
		assertFalse(arrayContains(result, pruneMe3));
		assertFalse(arrayContains(result, pruneMe4));

		assertEquals(4, result.length);
	}

	private boolean arrayContains(Object[] array, Object objectToLookFor) {
		for (Object object : array) {
			if (object == objectToLookFor) {
				return true;
			}
		}
		return false;
	}
	
	public void testFind_existingSitePresent_returnsInstallationSite() throws Exception {
		InstallationSite expectedSite = testee.create();
		expectedSite.addToConfiguredSites();
		
		InstallationSite actualSite = testee.find();
		
		assertEquals(expectedSite,actualSite);
		expectedSite.removeFromConfiguredSites();
	}
	
	public void testFind_noSiteExists_returnsNullInstallationSite() throws Exception {
		InstallationSite site = testee.find();
		assertTrue(site instanceof NullInstallationSite);
	}
		
	public void testCreate_creatingNewSite_leavesExistingDirectoriesUntouched() throws Exception {
		File testSubDir = new File(downloadRootDir,"rubbish");
		testSubDir.mkdir();
		
		testee.create();
		
		File[] subdirs = downloadRootDir.listFiles();
		
		assertEquals(2,subdirs.length);
		assertTrue(testSubDir.exists());
	}
	
	public void testCreate_creatingNewSite_makesDirectoryUnderDownloadRootDir() throws Exception {
		assertEquals(0,downloadRootDir.list().length);
		testee.create();
		
		File[] subdirs = downloadRootDir.listFiles();
		assertEquals(1,subdirs.length);
		assertTrue(subdirs[0].isDirectory());
		
		File[] subsubdirs = subdirs[0].listFiles();
		assertEquals(1,subsubdirs.length);
		assertTrue(subsubdirs[0].isDirectory());
		assertEquals("eclipse",subsubdirs[0].getName());
	}
		
	public void testFindCurrentDownloadSite_existingSitePresent_returnsSite() throws Exception {
		UpdateManagerSite expectedSite = (UpdateManagerSite) testee.create();
		expectedSite.addToConfiguredSites();
		IConfiguredSite actualSite = testee.findCurrentDownloadSite(downloadRootDir, SiteManager.getLocalSite().getCurrentConfiguration().getConfiguredSites());
		
		expectedSite.removeFromConfiguredSites();
		assertEquals(expectedSite.getConfiguredSite(),actualSite);
	}
	
	public void testFindCurrentDownloadSite_NoPreviouslyCreatedDownloadSites_returnsNullCurrentSite() throws Exception {
		IConfiguredSite[] onlyEclipseConfiguredSites = SiteManager.getLocalSite().getCurrentConfiguration().getConfiguredSites();
		IConfiguredSite actual = testee.findCurrentDownloadSite(downloadRootDir,onlyEclipseConfiguredSites);
		assertNull(actual);
	}
	
	public void testFindCurrentDownloadSite_NoConfiguredSites_returnsNullCurrentSite() throws Exception {
		IConfiguredSite actual = testee.findCurrentDownloadSite(downloadRootDir,new IConfiguredSite[0]);				
		assertNull(actual);
	}
	
	public void testIsSubdirectory_SourceIsNull_returnsFalse() {
		boolean isSubdirectory = testee.isSubdirectory(null, new File("c:\\windows"));
		assertFalse(isSubdirectory);		
	}
	
	public void testIsSubdirectory_TargetIsNull_returnsFalse() {
		boolean isSubdirectory = testee.isSubdirectory(new File("c:\\windows"), null);
		assertFalse(isSubdirectory);		
	}
	
	public void testIsSubdirectory_TargetIsSubdirectoryOfSource_returnsTrue() {		
		boolean isSubdirectory = testee.isSubdirectory(new File("c:\\windows"), new File("C:\\windows/system"));
		assertTrue(isSubdirectory);
	}

	public void testIsSubdirectory_TargetIsNotSubdirectoryOfSource_returnsFalse() {		
		boolean isSubdirectory = testee.isSubdirectory(new File("c:/windows"), new File("c:/temp"));
		assertFalse(isSubdirectory);
	}

	public void testIsSubdirectory_TargetIsSameAsParentButWithTrailingSlash_returnsFalse() {		
		boolean isSubdirectory = testee.isSubdirectory(new File("c:/windows"), new File("c:/windows/"));
		assertFalse(isSubdirectory);
	}

	public void testIsSubdirectory_TargetHasNoDirectorySeparator_returnsFalse() {		
		boolean isSubdirectory = testee.isSubdirectory(new File("c:/windows"), new File("c:/windowsrubbish"));
		assertFalse(isSubdirectory);
	}
	
	private boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }    
        return dir.delete();
    }	
}
