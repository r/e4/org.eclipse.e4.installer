/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.internal;

import java.util.List;

import junit.framework.TestCase;

import org.eclipse.e4.enterprise.installer.FeatureReferenceMother;
import org.eclipse.e4.enterprise.installer.FeatureVersionedIdentifier;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.IIncludedFeatureReference;

public class FeatureReferenceTreeTest extends TestCase {

	private FeatureReferenceTree testee;

	public void setUp() {
		testee = new FeatureReferenceTree();
	}

	public void testGetDescendants_GivenKnownKeysWithManyDescendants_returnsAllDescendants() throws Exception {

		// f1
		// ...f11 *
		// ......f111
		// ......f112
		// ......f113
		// .........f1131
		// ...f12
		// ......f121
		// ...f13
		// f2 *
		// ...f21
		// ...f22
		// ......f221
		// ......f222

		IIncludedFeatureReference f111 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f112 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f1131 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f113 = (IIncludedFeatureReference) FeatureReferenceMother.createFeatureWithChildren(f1131);
		
		FeatureVersionedIdentifier keyf11 = new FeatureVersionedIdentifier("f11", "11.11.11");
		IIncludedFeatureReference f11 = (IIncludedFeatureReference) FeatureReferenceMother.createFeatureReferenceFromFVIWithChildren(keyf11, f111, f112, f113);

		IIncludedFeatureReference f121 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f12 = (IIncludedFeatureReference )FeatureReferenceMother.createFeatureWithChildren(f121);
		
		IIncludedFeatureReference f13 = FeatureReferenceMother.createFeatureWithNoChildren();
		IFeatureReference f1 = FeatureReferenceMother.createFeatureWithChildren(f11, f12, f13);
		
		IIncludedFeatureReference f21 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f221 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f222 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f22 = (IIncludedFeatureReference)FeatureReferenceMother.createFeatureWithChildren(f221, f222);
		
		FeatureVersionedIdentifier keyf2 = new FeatureVersionedIdentifier("f2", "2.2.2");
		IFeatureReference f2 = FeatureReferenceMother.createFeatureReferenceFromFVIWithChildren(keyf2, f21,f22);

		testee.add(f1);
		testee.add(f2);
		
		List<IFeatureReference> descendantsF11 = testee.getFeatureReferenceWithDescendants(keyf11);
		assertEquals(5, descendantsF11.size());
		assertTrue(descendantsF11.contains(f11));
		assertTrue(descendantsF11.contains(f111));
		assertTrue(descendantsF11.contains(f112));
		assertTrue(descendantsF11.contains(f113));
		assertTrue(descendantsF11.contains(f1131));
		
		List<IFeatureReference> descendantsF2 = testee.getFeatureReferenceWithDescendants(keyf2);
		assertEquals(5, descendantsF2.size());
		assertTrue(descendantsF2.contains(f2));
		assertTrue(descendantsF2.contains(f21));
		assertTrue(descendantsF2.contains(f22));
		assertTrue(descendantsF2.contains(f221));
		assertTrue(descendantsF2.contains(f222));
	}

	public void testGetDescendants_GivenKnownKeyWithManyDescendants_returnsAllDescendants() throws Exception {

		IIncludedFeatureReference f4 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference f3 = (IIncludedFeatureReference) FeatureReferenceMother.createFeatureWithChildren(f4);

		FeatureVersionedIdentifier key = new FeatureVersionedIdentifier("foo", "1.2.3");
		IIncludedFeatureReference f2 = (IIncludedFeatureReference) FeatureReferenceMother.createFeatureReferenceFromFVIWithChildren(key, f3);
		IIncludedFeatureReference f1 = (IIncludedFeatureReference) FeatureReferenceMother.createFeatureWithChildren(f2);

		testee.add(f1);
	
		List<IFeatureReference> descendants = testee.getFeatureReferenceWithDescendants(key);
		assertEquals(3, descendants.size());
		assertTrue(descendants.contains(f2));
		assertTrue(descendants.contains(f3));
		assertTrue(descendants.contains(f4));

		
	}
	
	
	public void testGetDescendants_GivenKnownKeyWithNoDescendants_returnsTheReference() throws Exception {
		FeatureVersionedIdentifier key = new FeatureVersionedIdentifier("myFeature", "1.1.1");
		IFeatureReference keyReference = FeatureReferenceMother.createFeatureReferenceFromFVIWithNoChildren(key);
		testee.add(keyReference);

		List<IFeatureReference> descendants = testee.getFeatureReferenceWithDescendants(key);

		assertEquals(1, descendants.size());
		assertEquals(keyReference, descendants.get(0));
	}

	public void testGetDescendants_GivenUnknownKey_returnsEmptyList() throws Exception {
		List<IFeatureReference> descendants = testee.getFeatureReferenceWithDescendants(new FeatureVersionedIdentifier("missingfeature", "1.1.1"));
		assertTrue(descendants.isEmpty());
	}

	public void testWhenOneReferenceWithChildrenOfChildren_returnsAllFeatureReference() throws Exception {

		IIncludedFeatureReference grandChild1 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference grandChild2 = FeatureReferenceMother.createFeatureWithNoChildren();

		IFeatureReference child1 = FeatureReferenceMother.createFeatureWithChildren(grandChild1, grandChild2);

		IFeatureReference parent = FeatureReferenceMother.createFeatureWithChildren((IIncludedFeatureReference) child1);
		testee.add(parent);

		assertEquals(4, testee.getAllReferences().size());
		assertTrue(testee.getAllReferences().contains(parent));
		assertTrue(testee.getAllReferences().contains(child1));
		assertTrue(testee.getAllReferences().contains(grandChild1));
		assertTrue(testee.getAllReferences().contains(grandChild2));
	}

	public void testWhenOneReferenceWithMultipleChildren_returnsAll() throws Exception {
		IIncludedFeatureReference child1 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference child2 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference child3 = FeatureReferenceMother.createFeatureWithNoChildren();
		IIncludedFeatureReference child4 = FeatureReferenceMother.createFeatureWithNoChildren();
		IFeatureReference parent = FeatureReferenceMother.createFeatureWithChildren(child1, child2, child3, child4);
		testee.add(parent);

		assertEquals(5, testee.getAllReferences().size());
		assertTrue(testee.getAllReferences().contains(parent));
		assertTrue(testee.getAllReferences().contains(child1));
		assertTrue(testee.getAllReferences().contains(child2));
		assertTrue(testee.getAllReferences().contains(child3));
		assertTrue(testee.getAllReferences().contains(child4));
	}

	public void testWhenOneReferenceWithOneChildHasBeenAdded_returnsBoth() throws Exception {
		IIncludedFeatureReference child = FeatureReferenceMother.createFeatureWithNoChildren();
		IFeatureReference parent = FeatureReferenceMother.createFeatureWithChildren(child);
		testee.add(parent);

		assertEquals(2, testee.getAllReferences().size());
		assertTrue(testee.getAllReferences().contains(parent));
		assertTrue(testee.getAllReferences().contains(child));
	}

	public void testWhenOneReferenceWithNoChildrenHasBeenAdded_OnlyOneReturned() throws Exception {
		IFeatureReference expectedFeatureReference = FeatureReferenceMother.createFeatureWithNoChildren();
		testee.add(expectedFeatureReference);

		assertEquals(1, testee.getAllReferences().size());
		assertEquals(expectedFeatureReference, testee.getAllReferences().get(0));
	}

	public void testWhenNoObjectsHaveBeenAdded_EmptyListReturned() {
		assertTrue(testee.getAllReferences().isEmpty());
	}

}
