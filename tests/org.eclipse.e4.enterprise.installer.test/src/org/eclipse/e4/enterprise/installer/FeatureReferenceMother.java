/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.IIncludedFeatureReference;

public class FeatureReferenceMother {

	private static final String DEFAULT_ID = "id";
	private static final String DEFAULT_VERSION = "0.0.1";
	
	public static IFeatureReference createFeatureReferenceFromFVIWithChildren(FeatureVersionedIdentifier fvi, IIncludedFeatureReference... featureReference) {
		StubFeatureReference stub = new StubFeatureReference(new StubFeature(featureReference));
		stub.setVersionedIdentifier(fvi.featureID, fvi.featureVersion);		
		return stub;
	}

	public static IFeatureReference createFeatureReferenceFromFVIWithNoChildren(FeatureVersionedIdentifier fvi) {
		StubFeatureReference stub = new StubFeatureReference(new StubFeature(new StubFeatureReference[]{}));
		stub.setVersionedIdentifier(fvi.featureID, fvi.featureVersion);		
		return stub;
	}
	
	public static IIncludedFeatureReference createFeatureWithNoChildren() {
		StubFeatureReference stubFeatureReference = new StubFeatureReference(new StubFeature(new StubFeatureReference[]{}));
		stubFeatureReference.setVersionedIdentifier(DEFAULT_ID, DEFAULT_VERSION);
		return stubFeatureReference;
	}
	
	public static IFeatureReference createFeatureWithChildren(IIncludedFeatureReference... featureReference) {
		StubFeatureReference stubFeatureReference = new StubFeatureReference(new StubFeature(featureReference));
		stubFeatureReference.setVersionedIdentifier(DEFAULT_ID, DEFAULT_VERSION);
		return stubFeatureReference;
	}
	
	
	
	
}
