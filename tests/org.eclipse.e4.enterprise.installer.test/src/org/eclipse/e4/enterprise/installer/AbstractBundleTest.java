/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.eclipse.e4.enterprise.installer.internal.site.InstallationSite;
import org.eclipse.e4.enterprise.installer.internal.site.InstallationSiteManager;
import org.eclipse.e4.ui.test.utils.FixturesLocation;
import org.eclipse.e4.ui.test.utils.TestCaseX;


public abstract class AbstractBundleTest extends TestCaseX {

	public static final URL SITE400_FIXTURE_URL = makeFixtureURL("fixtures/site_4.0.0/site.xml");
	public static final URL SITE401_FIXTURE_URL = makeFixtureURL("fixtures/site_4.0.1/site.xml");
	public static final URL SITE500_FIXTURE_URL = makeFixtureURL("fixtures/site_5.0.0/site.xml");
	
	
	protected BundleUpdater testee;
	
	private static final String INSTALL_TO_SITE_DIR = System.getProperty("java.io.tmpdir") + File.separator + "AbstractBundleTest";
			
	protected File downloadRootDir;
		
	public AbstractBundleTest() {
		super();
	}

	@Override
	protected void setUp() throws Exception {
		//delete anything in the INSTALL_TO_SITE_DIR
		downloadRootDir = new File(INSTALL_TO_SITE_DIR);
		testee = new BundleUpdater();

		//if any tests are forced to stop AFTER the set-up but before the tearDown then we might have 
		//a problem, so we are just running tearDown first for safety
		tearDown();
	}

	@Override
	protected void tearDown() throws Exception {
		//find the site that exists and get rid of it
		InstallationSiteManager builder = new InstallationSiteManager(downloadRootDir);
		InstallationSite currentSite = builder.find();
		currentSite.removeFromConfiguredSites();
		deleteDir(downloadRootDir);
	}

	protected int countOccurancesOfListItemsStartingWithPrefix(List<String> list, String prefix) {
		int count = 0;
		for (String string : list) {
			if (string.startsWith(prefix))
				count++;
		}
		return count;
	}
	
	private static URL makeFixtureURL(String fixturePath) {
		// DJO: FIXME: Why does this ignore the fixturePath parameter?  This is smelly!  Phew!
		try {
//			return FileLocator.toFileURL(find);
			return new URL(FixturesLocation.FIXTURE_ROOT);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private boolean deleteDir(File fileOrDir) {
		if (fileOrDir.isDirectory()) {
			String[] children = fileOrDir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(fileOrDir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
	
		// The directory or file can now be deleted
		return fileOrDir.delete();
	}

}