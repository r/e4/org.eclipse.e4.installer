/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer;

import static org.eclipse.e4.enterprise.installer.BundleUpdaterConfig.DOWNLOAD_ROOT_KEY;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Set;

import org.eclipse.e4.enterprise.installer.BundleUpdaterConfig;
import org.eclipse.e4.enterprise.installer.FeatureVersionedIdentifier;
import org.eclipse.e4.enterprise.installer.InstallError;

import junit.framework.TestCase;


public class BundleUpdaterConfigTest extends TestCase {

	// convertToFeatureVersionSet
	// -------------------------------------------------------------------------------

	public void testConvertToFeatureVersionSet_MoreThan2SpaceSeparatedString_ThrowsException() throws Exception {
		String id = "featureID";
		String version = "1.2.3";
		String spurious = "spuriousExtraThing";

		String line = id + "  " + version + "  " + spurious;

		try {
			BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(line));
			fail();
		} catch (InstallError e) {
			// want to be here
		}

	}

	public void testConvertToFeatureVersionSet_InLineCommentsIgnored() throws Exception {
		String id1 = "featureID1";
		String version1 = "1.2.31";
		String id2 = "featureID2";
		String version2 = "1.2.32";
		String id3 = "featureID3";
		String version3 = "1.2.33";
		String id4 = "featureID4";
		String version4 = "1.2.34";

		String comment = "# I am a comment because I start with a hash.";

		String line1 = id1 + " " + version1 + comment + "\n";
		String line2 = id2 + "  " + version2 + comment + "\n";
		String line3 = id3 + "   " + version3 + comment + "\n";
		String line4 = id4 + "     " + version4 + comment + "\n";

		String input = line1 + line2 + line3 + line4;

		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(input));

		assertEquals(4, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id1, version1)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id2, version2)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id3, version3)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id4, version4)));
	}

	public void testConvertToFeatureVersionSet_CommentLinesIgnored() throws Exception {
		String id1 = "featureID1";
		String version1 = "1.2.31";
		String id2 = "featureID2";
		String version2 = "1.2.32";
		String id3 = "featureID3";
		String version3 = "1.2.33";
		String id4 = "featureID4";
		String version4 = "1.2.34";

		String line1 = id1 + " " + version1 + "\n";
		String line2 = id2 + "  " + version2 + "\n";
		String line3 = id3 + "   " + version3 + "\n";
		String line4 = id4 + "     " + version4 + "\n";

		String commentLine = "# I am a comment because I start with a hash.  \n";

		String input = line1 + commentLine + line2 + commentLine + line3 + commentLine + line4;

		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(input));

		assertEquals(4, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id1, version1)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id2, version2)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id3, version3)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id4, version4)));
	}

	public void testConvertToFeatureVersionSet_EmptyLinesIgnored() throws Exception {
		String id1 = "featureID1";
		String version1 = "1.2.31";
		String id2 = "featureID2";
		String version2 = "1.2.32";
		String id3 = "featureID3";
		String version3 = "1.2.33";
		String id4 = "featureID4";
		String version4 = "1.2.34";

		String line1 = id1 + " " + version1 + "\n";
		String line2 = id2 + "  " + version2 + "\n";
		String line3 = id3 + "   " + version3 + "\n";
		String line4 = id4 + "     " + version4 + "\n";

		String blankLine = "  \n";

		String input = line1 + blankLine + line2 + blankLine + line3 + blankLine + line4;

		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(input));

		assertEquals(4, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id1, version1)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id2, version2)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id3, version3)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id4, version4)));
	}

	public void testConvertToFeatureVersionSet_UnspecifiedVersion_defaultsTo000() throws Exception {
		String id = "featureID";
		String version = "";
		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(id + "  " + version));

		assertEquals(1, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id, "0.0.0")));
	}

	public void testConvertToFeatureVersionSet_ManyItems_returnsCorrectlyParsedSet() throws Exception {
		String id1 = "featureID1";
		String version1 = "1.2.31";
		String id2 = "featureID2";
		String version2 = "1.2.32";
		String id3 = "featureID3";
		String version3 = "1.2.33";
		String id4 = "featureID4";
		String version4 = "1.2.34";

		String line1 = id1 + " " + version1 + "\n";
		String line2 = id2 + "  " + version2 + "\n";
		String line3 = id3 + "   " + version3 + "\n";
		String line4 = id4 + "     " + version4 + "\n";

		String input = line1 + line2 + line3 + line4;

		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(input));

		assertEquals(4, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id1, version1)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id2, version2)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id3, version3)));
		assertTrue(result.contains(new FeatureVersionedIdentifier(id4, version4)));
	}

	public void testConvertToFeatureVersionSet_singleItem_returnsCorrectlyParsedSet() throws Exception {
		String id = "featureID";
		String version = "1.2.3";
		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(id + "  " + version));

		assertEquals(1, result.size());
		assertTrue(result.contains(new FeatureVersionedIdentifier(id, version)));
	}

	public void testConvertToFeatureVersionSet_EmptyStream_returnsEmptySet() throws Exception {
		Set<FeatureVersionedIdentifier> result = BundleUpdaterConfig.convertToFeatureVersionSet(convertStringToStream(""));

		assertEquals(0, result.size());
	}

	private ByteArrayInputStream convertStringToStream(String contents) {
		ByteArrayInputStream inputStream = new ByteArrayInputStream(contents.getBytes());
		return inputStream;
	}

	// parseURLs
	// -------------------------------------------------------------------------------

	public void testParseURLS_WithAtLeastOneDuffOne_throwsException() throws Exception {
		String urlAsString1 = "http://www.eclipse.org";
		String urlAsString2 = "http://news.bbc.co.uk";
		String urlAsString3 = "I-am-not-a-valid-url";
		String urlAsString4 = "http://www.theregister.co.uk/";

		String input = urlAsString1 + " " + urlAsString2 + "    " + urlAsString3 + "     " + urlAsString4 + "   ";

		try {
			BundleUpdaterConfig.parseURLs(input);
			fail();
		} catch (InstallError e) {
			assertTrue(e.getCause() instanceof MalformedURLException);
		}
	}

	public void testParseURLS_N_ValidURLsWithVariousSpaces_parsedCorrectly() throws Exception {
		String urlAsString1 = "http://www.eclipse.org";
		String urlAsString2 = "http://news.bbc.co.uk";
		String urlAsString3 = "http://slashdot.org/";
		String urlAsString4 = "http://www.theregister.co.uk/";

		String s = " ";

		String input = s + s + s + urlAsString1 + s + urlAsString2 + s + s + s + urlAsString3 + s + s + s + s + s + urlAsString4 + s + s;

		URL[] result = BundleUpdaterConfig.parseURLs(input);

		assertEquals(4, result.length);
		assertEquals(urlAsString1, result[0].toString());
		assertEquals(urlAsString2, result[1].toString());
		assertEquals(urlAsString3, result[2].toString());
		assertEquals(urlAsString4, result[3].toString());
	}

	public void testParseURLS_TwoValidURLsWithManySpaces_parsedCorrectly() throws Exception {
		String urlAsString1 = "http://www.eclipse.org";
		String urlAsString2 = "http://news.bbc.co.uk";

		String fourSpaces = "    ";
		String input = urlAsString1 + fourSpaces + urlAsString2;

		URL[] result = BundleUpdaterConfig.parseURLs(input);

		assertEquals(2, result.length);
		assertEquals(urlAsString1, result[0].toString());
		assertEquals(urlAsString2, result[1].toString());
	}

	public void testParseURLS_TwoValidURLs_parsedCorrectly() throws Exception {
		String urlAsString1 = "http://www.eclipse.org";
		String urlAsString2 = "http://news.bbc.co.uk";

		String input = urlAsString1 + " " + urlAsString2;

		URL[] result = BundleUpdaterConfig.parseURLs(input);

		assertEquals(2, result.length);
		assertEquals(urlAsString1, result[0].toString());
		assertEquals(urlAsString2, result[1].toString());
	}

	public void testParseURLS_SingleURLWithPadding_parsedCorrectly() throws Exception {
		String urlAsString = "  http://www.eclipse.org    ";

		URL[] result = BundleUpdaterConfig.parseURLs(urlAsString);

		assertEquals(1, result.length);
		assertEquals(urlAsString.trim(), result[0].toString());
	}

	public void testParseURLS_SingleURL_parsedCorrectly() throws Exception {
		String urlAsString = "http://www.eclipse.org";

		URL[] result = BundleUpdaterConfig.parseURLs(urlAsString);

		assertEquals(1, result.length);
		assertEquals(urlAsString, result[0].toString());
	}

	public void testParseURLS_KeyPresentValueIsSpace_throwsException() throws Exception {
		try {
			BundleUpdaterConfig.parseURLs(" ");
			fail();
		} catch (InstallError e) {
			assertTrue(e.getCause() instanceof MalformedURLException);
		}
	}

	public void testParseURLS_KeyPresentValueIsEmptyString_throwsException() throws Exception {
		try {
			BundleUpdaterConfig.parseURLs("");
			fail();
		} catch (InstallError e) {
			assertTrue(e.getCause() instanceof MalformedURLException);
		}
	}

	public void testParseURLS_KeyPresentButNoValue_ThrowsException() throws Exception {
		try {
			BundleUpdaterConfig.parseURLs(null);
			fail();
		} catch (InstallError e) {
			// want to be here
		}
	}

	public void testFindDownloadDirectoryRoot_PropertySet_returnsValueOfProperty() throws Exception {
		String expectedPath = "c:/temp/mytestvalue";
		Properties properties = new Properties();
		properties.put(DOWNLOAD_ROOT_KEY, expectedPath);
		BundleUpdaterConfig.props = properties; // inject our properties into
		// BundleUpdaterConfig

		File actual = BundleUpdaterConfig.findDownloadDirectoryRoot();

		File expected = new File(expectedPath);

		assertEquals(expected, actual);
	}

	public void testFindDownloadDirectoryRoot_NoPropertySet_returnsDefaultDownloadRoot() throws Exception {
		BundleUpdaterConfig.props = new Properties();

		File actual = BundleUpdaterConfig.findDownloadDirectoryRoot();
		File expected = BundleUpdaterConfig.getDefaultDownloadRoot();

		assertEquals(expected, actual);
	}
}
