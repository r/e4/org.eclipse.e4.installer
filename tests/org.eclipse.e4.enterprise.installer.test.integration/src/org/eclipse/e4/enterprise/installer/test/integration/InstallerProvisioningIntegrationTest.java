/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class InstallerProvisioningIntegrationTest extends IntegrationTestCase {

	// This is the eclipse app that the tests will use
	private static final String PRODUCT_DIR = "C:/temp/exported_installer_integration_test_product";
	private static final String PRODUCT_EXE_DIR = PRODUCT_DIR + "/eclipse";

	private static final String PROVISIONING_FILENAME = PRODUCT_EXE_DIR + "/provisioning.txt";
	
	private static final String DOWNLOAD_DIR = PRODUCT_EXE_DIR + "/iit";
	private static String MESSAGE_FILENAME_AND_PATH = DOWNLOAD_DIR + "/message-file"; // TODO:
	private static final String MESSAGE_FILENAME_SUFFIX = ".txt";

	private static final String CONFIGURATION_FILENAME = "configuration.ini";

	private File backupOriginalIni;
	private File configini = new File(PRODUCT_EXE_DIR + "/" + CONFIGURATION_FILENAME);

	public InstallerProvisioningIntegrationTest() throws IOException {
		// back up the ini file so it can be restored during setup.
		// why? Because the tests fiddle with it.
		backupOriginalIni = File.createTempFile("orginial-config", "ini");
		copyFile(configini, backupOriginalIni);

		System.out.println("back up is here: [" + backupOriginalIni.getAbsolutePath() + "]");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		copyFile(backupOriginalIni, configini);
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.setUp();
		copyFile(backupOriginalIni, configini); 
	}
	
	public void testLocalCopyIsBeingUsed() throws Exception {

		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// install features DEF from remote site.
		String line1 = FIXTURE_FEATURE_ID_D + " 1.0.0";
		String line2 = FIXTURE_FEATURE_ID_E + " 1.0.0";
		String line3 = FIXTURE_FEATURE_ID_F + " 1.0.0";
		String[] lines = new String[] { line1, line2, line3 };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_DEF_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "f.feature"));
		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

		// now install features ABCDEF but use the corrupted remote site for
		// DEF. If the feature E is installed properly it will show the
		// (previously downloaded) local copy was used.

		String line4 = FIXTURE_FEATURE_ID_A + " 1.0.0";
		String line5 = FIXTURE_FEATURE_ID_B + " 1.0.0";
		String line6 = FIXTURE_FEATURE_ID_C + " 1.0.0";

		String[] newLines = new String[] { line1, line2, line3, line4, line5, line6 };
		writeProvisioningInfoToNewFile(newLines, PROVISIONING_FILENAME);

		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE + " "
				+ SITE_DEF_CORRUPTED_FIXTURE, false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX,
				PRODUCT_EXE_DIR, configini);

		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "c.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "f.feature"));
		assertEquals(6, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix));

	}


	public void testUnjarredFeaturesAreProperlyCopied() throws Exception {

		// this test relies on other tests to verify that local copies are used
		// "testLocalCopyIsBeingUsed()".
		//
		// This test is verifying that a feature that demands to have its JARs
		// expanded is treated correctly in the whole "copy from local area"
		// process.

		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// install feature that will be expanded on install
		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture";
		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_FIXTURE_EXPANDED_JAR, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, FIXTURE_EXPANDED_JAR_FEATURE));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

		// install additional feature and verify that expanded feature is copied
		String fixtureList = SITE_FIXTURE_EXPANDED_JAR + "  " + SITE401_FIXTURE;
		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(fixtureList, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, FIXTURE_EXPANDED_JAR_FEATURE));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures,
				"org.eclipse.e4.enterprise.installer.test.fixture.feature4.0.1"));
		assertEquals(2, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix));
	}

	public void testProvisionSubsetOfFeaturesFromPossiblesAcrossManySitesWithSomeAlreadyInstalledLocally()
			throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		String lineA = FIXTURE_FEATURE_ID_A + " 1.0.0";
		String lineB = FIXTURE_FEATURE_ID_B + " 1.0.0";
		String lineD = FIXTURE_FEATURE_ID_D + " 1.0.0";
		String lineE = FIXTURE_FEATURE_ID_E + " 1.0.0";

		// provision features ABDE
		String[] lines = new String[] { lineA, lineB, lineD, lineE, };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE + " " + SITE_DEF_FIXTURE,
				false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));
		assertEquals(4, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

		// provision features ABCDE (ie add "C"), but use corrupt site for DE
		// which will show that these are copied locally.

		String lineC = FIXTURE_FEATURE_ID_C + " 1.0.0";

		String[] newLines = new String[] { lineA, lineB, lineC, lineD, lineE };
		writeProvisioningInfoToNewFile(newLines, PROVISIONING_FILENAME);

		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE + " "
				+ SITE_DEF_CORRUPTED_FIXTURE, false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX,
				PRODUCT_EXE_DIR, configini);

		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "c.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "e.feature"));
		assertEquals(5, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix));
	}
	
	public void testWhenRequestingASingleFeatureButNotSpecifyingTheVersion_thatOnlyOneInstanceOfTheFeatureWillBeProvisioned() throws Exception {
		// reset the state to S(0)
		String featureID = "org.eclipse.e4.enterprise.installer.test.fixture.qualifier.feature";
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(SITE_QUALIFIER_FIXTURE, "1.0.0.M7", 
				false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini, featureID);		
		
		String[] lines = {featureID + " 0.0.0"};
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);
		addProvisioningToConfig(configini, PROVISIONING_FILENAME);
		
		String fixtureList = SITE_QUALIFIER_MULTIPLE_VERSIONS_FIXTURE;
		List<String> features = installFromFixturesReturnInstalledFeatures(fixtureList, false, 
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID) );
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID+"1.0.0.R3") );
		
	}
	
	public void testProvisionManyVersionsOfSameFeature() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featureID = "org.eclipse.e4.enterprise.installer.test.fixture.feature";

		String line1 = featureID + " 4.0.0";
		String line2 = featureID + " 4.0.1";
		String line3 = featureID + " 5.0.0";

		String[] lines = new String[] { line1, line2, line3 };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		String fixtureList = SITE400_FIXTURE + " " + SITE401_FIXTURE + " " + SITE500_FIXTURE;
		List<String> features = installFromFixturesReturnInstalledFeatures(fixtureList, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features, featureID));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "4.0.0"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "4.0.1"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "5.0.0"));
	}
	
	public void testProvisionNonExistentFeatureRollsBackToInitialState() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// this should fail and thus roll back to S(0).
		String line1 = FIXTURE_FEATURE_ID_D + " 1.0.0";
		String line2 = "I-am-a-nonsense-feature" + " 1.0.0";
		String line3 = FIXTURE_FEATURE_ID_F + " 1.0.0";
		String[] lines = new String[] { line1, line2, line3 };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_DEF_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String prefixprefix = "org.eclipse.e4.enterprise.installer.test.fixture.";
		String notInstalledPrefix = prefixprefix + "many_";
		String installedPrefix = prefixprefix + "feature";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, installedPrefix + "4.0.1"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, notInstalledPrefix + "d.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, notInstalledPrefix + "f.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, prefixprefix));
	}
	
	public void testDeProvisioning() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		// provision features ABCDEF
		String lineA = FIXTURE_FEATURE_ID_A + " 1.0.0";
		String lineB = FIXTURE_FEATURE_ID_B + " 1.0.0";
		String lineC = FIXTURE_FEATURE_ID_C + " 1.0.0";
		String lineD = FIXTURE_FEATURE_ID_D + " 1.0.0";
		String lineE = FIXTURE_FEATURE_ID_E + " 1.0.0";
		String lineF = FIXTURE_FEATURE_ID_F + " 1.0.0";
		String[] lines = new String[] { lineA, lineB, lineC, lineD, lineE, lineF };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE + " " + SITE_DEF_FIXTURE,
				false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "c.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "f.feature"));
		assertEquals(6, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

		// deprovision features B, D and F
		String[] newLines = new String[] { lineA, lineC, lineE };
		writeProvisioningInfoToNewFile(newLines, PROVISIONING_FILENAME);

		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(
				SITE_ABC_FIXTURE + " " + SITE_DEF_FIXTURE, false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX,
				PRODUCT_EXE_DIR, configini);

		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "c.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "e.feature"));
		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix));
	}
	
	public void testMissingVersionNumbersInProvisioningAreInferred() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String line1 = FIXTURE_FEATURE_ID_D; // note no version
		String line2 = FIXTURE_FEATURE_ID_E;
		String line3 = FIXTURE_FEATURE_ID_F;
		String[] lines = new String[] { line1, line2, line3 };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_DEF_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "f.feature"));
		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

	}
	
	public void testVersionNumber000ManagedCorrectly() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String line1 = FIXTURE_FEATURE_ID_D + " 0.0.0"; // note 0.0.0
		String line2 = FIXTURE_FEATURE_ID_E + " 0.0.0";
		String line3 = FIXTURE_FEATURE_ID_F + " 0.0.0";
		String[] lines = new String[] { line1, line2, line3 };
		writeProvisioningInfoToNewFile(lines, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_DEF_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "f.feature"));
		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));
	}

	public void testProvision2FeaturesFromPossiblesAcrossManySites() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		writeProvisioningInfoToNewFile(new String[] { (FIXTURE_FEATURE_ID_A + " 1.0.0"),
				(FIXTURE_FEATURE_ID_F + " 1.0.0") }, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE + " " + SITE_DEF_FIXTURE,
				false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "f.feature"));

		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "b.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "c.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "d.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "e.feature"));

	}

	public void testProvision2featuresFromPossible3InSingleRemoteSite() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		writeProvisioningInfoToNewFile(new String[] { (FIXTURE_FEATURE_ID_A + " 1.0.0"),
				(FIXTURE_FEATURE_ID_C + " 1.0.0") }, PROVISIONING_FILENAME);

		addProvisioningToConfig(configini, PROVISIONING_FILENAME);

		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "c.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "b.feature"));
	}
	
	

}
