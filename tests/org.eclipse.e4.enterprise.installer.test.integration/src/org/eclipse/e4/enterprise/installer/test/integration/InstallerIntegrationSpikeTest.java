/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

/**
 * This class contains exploratory & spike code used to determine the behaviour 
 * of Eclipse during the upgrade/installer process. It is captured as comments
 * just now to preserve the thought that went into the work. 
 */
public class InstallerIntegrationSpikeTest extends IntegrationTestCase {

	
	public void testToPreventFailuresForNoTestsInTheClass()
	{		
	}
	
	// public void test() throws Exception {
	// // include some extra stuff in the product def prior to export
	// // try to move the extra stuff from EID to DLS, expect failur
	// // update the .product def to omit the extra stuff
	// // try to move the extra stuff from EID to DLS, expect success.
	//
	// fail("write this test!");
	// }

	// public void
	// testMovingFeaturesAndPluginsBetweenConfiguredSitesViaTheFileSystem()
	// throws Exception {
	// /*
	// * Next test: See if we can move Config plugin from EID into DLS. IF
	// * yes: Explore parameters of what we can move IF no: See if some other
	// * strategy makes it work. In particular: - Can we deconfigure a running
	// * feature/bundle, move it, and reconfigure it? - Can we copy it to the
	// * DLS, restart, then delete it from the EID?
	// */
	//
	// // terminology:
	// // EID = eclipse installation directory
	// // DLR = down load root
	// // DLS = down load site, typically a sub-folder of the DLR
	// File eidFeatures = new File(PRODUCT_EXE_DIR + "/features/");
	// File eidPlugins = new File(PRODUCT_EXE_DIR + "/plugins/");
	//
	// //
	// ----------------------------------------------------------------------------------
	// // Test #1 - Non Product Features
	// //
	// ----------------------------------------------------------------------------------
	//
	// // need to roll back to 4.1.1
	// // why? to install extra non-product stuff into Eclipse folders
	// initializeProduct("4.1.1");
	//
	// int initialFeatureCount = eidFeatures.listFiles().length;
	// int initialPluginCount = eidPlugins.listFiles().length;
	//
	// //
	// ----------------------------------------------------------------------------------
	// // install some non-product stuff into Eclipse dirs
	//
	// testHarnessPath = PRODUCT_EXE_DIR;
	// configini = new File(PRODUCT_EXE_DIR + "/" + CONFIGURATION_FILENAME);
	// MESSAGE_FILENAME_AND_PATH = PRODUCT_EXE_DIR + "/message-file";
	//
	// List<String> features =
	// installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE, true);
	//
	// // so at this point, we should have one config site == EID with a known
	// // number of features installed;
	// assertInitialFeatureSet(features);
	//
	// // note: the test harness doesn't count non E4 features, so the RCP
	// // feature is installed, but not listed in this test.
	// assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.rcp"));
	// // ... thus the above assertion is only there to explain to you, dear
	// // reader, why it isn't being counted, lest you look at the file-system
	// // and be perturbed or confuzzed in any way.
	//
	// //
	// ----------------------------------------------------------------------------------
	// // move (via FS) stuff from EID to DLS
	//
	// // first, we need a new DLS site that isn't the EID. To do that, we
	// // upgrade to the latest version of the installer and use it to install
	// // some other, non prod features (DEF)
	//
	// // note: this installation will still put all features into the EID
	// features =
	// installFromFixturesReturnInstalledFeatures(URL_UPGRADE_UPDATESITE, true);
	//
	// // We should still have the same set of features as before
	// assertInitialFeatureSet(features);
	//
	// // now we install some additional non-product features, DEF (into new
	// // DLS):
	// features = installAndPauseAndReturnFeatures(SITE_DEF_FIXTURE);
	//
	// // now we have an additional set of features:
	// assertFinalFeatureSetAvailable(features);
	//
	// // At this point:
	// // 1. the ABC features (and the original product features AND the new
	// // version of the installer/config) are installed
	// // in the EID.
	// // 2. the DEF features are in the new DLS.
	//
	// // check the features and plugins are where we think they are
	// // note: in ABCDEF there is a 1:1 mapping between features and plugins.
	// // i.e each has 1 feature containing 1 plugin.
	//
	// // features:
	// File dlr = new File(PRODUCT_EXE_DIR + "/InstallerDownloadRoot/");
	// File[] dlrContents = dlr.listFiles();
	// assertEquals(1, dlrContents.length);
	// File dls = dlrContents[0];
	//
	// File dlsFeatures = new File(dls, "eclipse/features");
	// File dlsPlugins = new File(dls, "eclipse/plugins/");
	//
	// final int upgradedInstallerAndConfigFeatures = 2;
	// final int abc = 3;
	// int expectedFeaturesInEID = initialFeatureCount +
	// upgradedInstallerAndConfigFeatures + abc;
	//
	// final int def = 3;
	// int expectedFeaturesInDLS = def;
	//
	// assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	//
	// // plugins:
	// // config + installer + installer.ui.swt
	// final int upgradedInstallerAndConfigPlugins = 3;
	// int expectedPluginsInEID = initialPluginCount +
	// upgradedInstallerAndConfigPlugins + abc;
	// int expectedPluginsInDLS = def;
	//
	// assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// // so now we can move the features ABC from the EID to the DLS and check
	// // everything still works ok.
	//
	// // move the ABC features from EID to DLS
	// File featureA_inEID = new File(eidFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_a.feature_1.0.0");
	// File featureB_inEID = new File(eidFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_b.feature_1.0.0");
	// File featureC_inEID = new File(eidFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_c.feature_1.0.0");
	//
	// File featureA_inDLS = new File(dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_a.feature_1.0.0");
	// File featureB_inDLS = new File(dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_b.feature_1.0.0");
	// File featureC_inDLS = new File(dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_c.feature_1.0.0");
	//
	// moveFileOrDirectory(featureA_inEID, featureA_inDLS);
	// moveFileOrDirectory(featureB_inEID, featureB_inDLS);
	// moveFileOrDirectory(featureC_inEID, featureC_inDLS);
	//
	// // move the ABC plugins from EID to DLS
	//
	// File pluginA_inEID = new File(eidPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_a_1.0.0.jar");
	// File pluginB_inEID = new File(eidPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_b_1.0.0.jar");
	// File pluginC_inEID = new File(eidPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_c_1.0.0.jar");
	//
	// File pluginA_inDLS = new File(dlsPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_a_1.0.0.jar");
	// File pluginB_inDLS = new File(dlsPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_b_1.0.0.jar");
	// File pluginC_inDLS = new File(dlsPlugins,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many_c_1.0.0.jar");
	//
	// moveFileOrDirectory(pluginA_inEID, pluginA_inDLS);
	// moveFileOrDirectory(pluginB_inEID, pluginB_inDLS);
	// moveFileOrDirectory(pluginC_inEID, pluginC_inDLS);
	//
	// // _double_ check that our features+plugins have been moved to the right
	// // place.
	// expectedFeaturesInEID = initialFeatureCount +
	// upgradedInstallerAndConfigFeatures;
	// expectedFeaturesInDLS = def + abc;
	//
	// assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	//
	// expectedPluginsInEID = initialPluginCount +
	// upgradedInstallerAndConfigPlugins;
	// expectedPluginsInDLS = def + abc;
	//
	// assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// startAppDownloadNothingGetFeatures(dlr);
	//
	// // _triple_ check that our features+plugins have been moved to the right
	// // place.
	// expectedFeaturesInEID = initialFeatureCount +
	// upgradedInstallerAndConfigFeatures;
	// expectedFeaturesInDLS = def + abc;
	//
	// assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	//
	// expectedPluginsInEID = initialPluginCount +
	// upgradedInstallerAndConfigPlugins;
	// expectedPluginsInDLS = def + abc;
	//
	// assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// // ...and that we still have the same set of features as before
	// assertFinalFeatureSetAvailable(features);
	//
	// //
	// ----------------------------------------------------------------------------------
	// // Test #2 - Product Feature (singular): "Config" (ie something in the
	// // .product file).
	// //
	// ----------------------------------------------------------------------------------
	//
	// moveFromEIDToDLS(eidFeatures, dlsFeatures,
	// "org.eclipse.e4.enterprise.config.feature_4.0.0.200910290146");
	// moveFromEIDToDLS(eidFeatures, dlsFeatures,
	// "org.eclipse.e4.enterprise.config.feature_4.0.0.200910281309");
	//
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.config_4.0.0.200910290146.jar");
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.config_4.0.0.200910281309.jar");
	//
	// // run up the app, and check which features are installed.
	// features = startAppDownloadNothingGetFeatures(dlr);
	//
	// // double check that our features have been moved to the right place.
	// // the "minus 1" is the config feature
	// expectedFeaturesInEID = (initialFeatureCount - 1) +
	// (upgradedInstallerAndConfigFeatures - 1);
	// expectedFeaturesInDLS = def + abc + 2;
	// assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	//
	// expectedPluginsInEID = (initialPluginCount - 1) +
	// (upgradedInstallerAndConfigPlugins - 1);
	// expectedPluginsInDLS = def + abc + 2;
	// assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// // Check that we still have the same set of features as before
	// assertFinalFeatureSetAvailable(features);
	//
	// //
	// ----------------------------------------------------------------------------------
	// // Test #3 - Product Features
	// //
	// ----------------------------------------------------------------------------------
	//
	// moveFromEIDToDLS(eidFeatures, dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.feature_4.0.0.200910281309");
	// moveFromEIDToDLS(eidFeatures, dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.feature_4.4.1.200910290146");
	//
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.installer.ui.swt_1.0.0.jar");
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.installer_4.0.0.200910281309.jar");
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.installer_4.4.1.200910290146.jar");
	//
	// // run up the app, and check which features are installed.
	// features = startAppDownloadNothingGetFeatures(dlr);
	//
	// // double check that our features have been moved to the right place.
	// // The "2" is the installer and config from the original installation.
	// expectedFeaturesInEID = initialFeatureCount - 2;
	// expectedFeaturesInDLS = def + abc + upgradedInstallerAndConfigFeatures +
	// 2;
	// assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	//
	// expectedPluginsInEID = initialPluginCount - 2;
	// expectedPluginsInDLS = def + abc + upgradedInstallerAndConfigPlugins + 2;
	// assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// // Check that we still have the same set of features as before
	// assertFinalFeatureSetAvailable(features);
	//
	// //
	// ----------------------------------------------------------------------------------
	// // Test #4 - Move everything else... :-O
	// //
	// ----------------------------------------------------------------------------------
	//
	// moveFromEIDToDLS(eidFeatures, dlsFeatures,
	// "org.eclipse.e4.enterprise.installer.test.integration.rcpmain.feature_1.0.0");
	// moveFromEIDToDLS(eidPlugins, dlsPlugins,
	// "org.eclipse.e4.enterprise.installer.test.integration.rcpmain_1.0.0.jar");
	//
	// // run up the app, and check which features are installed.
	// features = startAppDownloadNothingGetFeatures(dlr);
	//
	// // double check that our features have been moved to the right place.
	// // The "2" is the installer and config from the original installation.
	// // expectedFeaturesInEID = initialFeatureCount - 2;
	// // expectedFeaturesInDLS = def + abc + upgradedInstallerAndConfigFeatures
	// + 2;
	// // assertEquals(expectedFeaturesInEID, eidFeatures.listFiles().length);
	// // assertEquals(expectedFeaturesInDLS, dlsFeatures.listFiles().length);
	// //
	// // expectedPluginsInEID = initialPluginCount - 2;
	// // expectedPluginsInDLS = def + abc + upgradedInstallerAndConfigPlugins +
	// 2;
	// // assertEquals(expectedPluginsInEID, eidPlugins.listFiles().length);
	// // assertEquals(expectedPluginsInDLS, dlsPlugins.listFiles().length);
	//
	// // Check that we still have the same set of features as before
	// assertFinalFeatureSetAvailable(features);
	// }
	//
	// private List<String> startAppDownloadNothingGetFeatures(File dlr) throws
	// IOException, Exception, InterruptedException {
	// // Force it not to not do any work by thinking we are on a restart
	// RestartManager restartManager = new RestartManager(dlr);
	// restartManager.restartInitiated();
	//
	// String fixtureListThatWillBeIgnored = SITE_ABC_FIXTURE;
	// return installAndPauseAndReturnFeatures(fixtureListThatWillBeIgnored);
	// }
	//
	// private void moveFromEIDToDLS(File eidParent, File dlsParent, String
	// nameOnDisk) throws IOException {
	// moveFileOrDirectory(new File(eidParent, nameOnDisk), new File(dlsParent,
	// nameOnDisk));
	// }
	//
	// private List<String> installAndPauseAndReturnFeatures(String
	// fixtureListThatWillBeIgnored) throws Exception, InterruptedException {
	// List<String> features;
	// features =
	// installFromFixturesReturnInstalledFeatures(fixtureListThatWillBeIgnored,
	// true);
	// int seconds = 3;
	// System.out.println("sleeping for " + seconds + " seconds....");
	// Thread.sleep(seconds * 1000);
	// System.out.println("Awake!");
	// return features;
	// }
	//
	// private void printOut(String[] list, String mssg) {
	// System.out.println(mssg);
	// Arrays.sort(list);
	// for (int i = 0; i < list.length; i++) {
	// System.out.println("\t" + list[i]);
	// }
	// }
	//
	// private void assertInitialFeatureSet(List<String> features) {
	// assertEquals(6, features.size());
	// // the stuff that constitutes the product:
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.test.integration.rcpmain.feature"));
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.config.feature"));
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.feature"));
	// // also the stuff we poured in after:
	// assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many"));
	// }
	//
	// private void assertFinalFeatureSetAvailable(List<String> features) {
	// assertEquals(9, features.size());
	// // the stuff that constitutes the product:
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.test.integration.rcpmain.feature"));
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.config.feature"));
	// assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.feature"));
	// // also the stuff we poured in after:
	// assertEquals(6, countOccurancesOfListItemsStartingWithPrefix(features,
	// "org.eclipse.e4.enterprise.installer.test.fixture.many"));
	// }
	//
	// private void moveFileOrDirectory(File source, File target) throws
	// IOException {
	// copyDirectory(source, target);
	// assertTrue(recursiveDelete(source));
	// }
	//
	// private void copyDirectory(File sourceLocation, File targetLocation)
	// throws IOException {
	//
	// if (sourceLocation.isDirectory()) {
	// if (!targetLocation.exists()) {
	// targetLocation.mkdir();
	// }
	//
	// String[] children = sourceLocation.list();
	// for (int i = 0; i < children.length; i++) {
	// copyDirectory(new File(sourceLocation, children[i]), new
	// File(targetLocation, children[i]));
	// }
	// } else {
	//
	// InputStream in = new FileInputStream(sourceLocation);
	// OutputStream out = new FileOutputStream(targetLocation);
	//
	// // Copy the bits from instream to outstream
	// byte[] buf = new byte[1024];
	// int len;
	// while ((len = in.read(buf)) > 0) {
	// out.write(buf, 0, len);
	// }
	// in.close();
	// out.close();
	// }
	// }

	// Exploratory-Work-<end>--------------------------------------------------------------------------------------------------------------

	
}
