/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.e4.enterprise.installer.BundleUpdaterConfig;
import org.eclipse.e4.ui.test.utils.FixturesLocation;
import org.eclipse.e4.ui.test.utils.TestCaseX;
import org.eclipse.e4.ui.test.utils.UnZip;


public class IntegrationTestCase extends TestCaseX {

	// Mount your code directory to the O: drive using 'subst'
	protected static final String WORKSPACE_ROOT = "file:///o:/trunk/code/";

	protected static final String URL_UPGRADE_UPDATESITE = WORKSPACE_ROOT
			+ "org.eclipse.e4.enterprise.installer.test.integration.rcpmain.updatesite/";
	protected static final String URL_UPGRADE_PRODUCTS = WORKSPACE_ROOT
			+ "org.eclipse.e4.enterprise.installer.test.integration.rcpmain.fixture/";

	protected static final String SITE400_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.0/site.xml";
	protected static final String SITE401_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.1/site.xml";
	protected static final String SITE500_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_5.0.0/site.xml";
	protected static final String SITE509_NONEXISTANT_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_5.0.9_nonexistant/site.xml";
	protected static final String SITE500_BROKEN_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_5.0.0_missingplugins/site.xml";
	protected static final String SITE_FIXTURE_EXPANDED_JAR = FixturesLocation.FIXTURE_ROOT + "/org.eclipse.e4.enterprise.installer.test.fixture.many_a.expanded.updatesite/site.xml";
	protected static final String SITE_QUALIFIER_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_qualifier/site.xml";
	protected static final String SITE_QUALIFIER_MULTIPLE_VERSIONS_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_qualifier_multiple_versions/site.xml";
	
	protected static final String FIXTURE_EXPANDED_JAR_FEATURE = "org.eclipse.e4.enterprise.installer.test.fixture.many_a.expanded.feature";
	protected static final String FIXTURE_FEATURE_ID_A = "org.eclipse.e4.enterprise.installer.test.fixture.many_a.feature";
	protected static final String FIXTURE_FEATURE_ID_B = "org.eclipse.e4.enterprise.installer.test.fixture.many_b.feature";
	protected static final String FIXTURE_FEATURE_ID_C = "org.eclipse.e4.enterprise.installer.test.fixture.many_c.feature";
	protected static final String FIXTURE_FEATURE_ID_D = "org.eclipse.e4.enterprise.installer.test.fixture.many_d.feature";
	protected static final String FIXTURE_FEATURE_ID_E = "org.eclipse.e4.enterprise.installer.test.fixture.many_e.feature";
	protected static final String FIXTURE_FEATURE_ID_F = "org.eclipse.e4.enterprise.installer.test.fixture.many_f.feature";
	
	protected static final String SITE_ABC_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_ABC/site.xml";
	protected static final String SITE_DEF_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_DEF/site.xml";
	protected static final String SITE_DEF_CORRUPTED_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_DEF-corrupted/site.xml";

	// This constant is the location from the rcpmainexample.Application MESSAGE_LOCATION_KEY
	// Perhaps we should have a proper dependency on it, but for now, just make sure this string is in sync.
	private static final String RCPEXAMPLE_MESSAGE_LOCATION_KEY = "org.eclipse.e4.enterprise.installer.test.integration.MESSAGE_LOCATION";
	
	/*
	 * Initialize/refresh the product folder with a newly downloaded product
	 * fixture
	 */
	protected void initializeProduct(String version, String productDirectory) throws Exception {
		File productDir = new File(productDirectory);
		recreateDir(productDir);

		URL productZipFileURL = new URL(URL_UPGRADE_PRODUCTS + version + "/eclipse.zip");
		new UnZip(productDir).unZip(productZipFileURL.openStream());
	}

	protected List<String> runTestHarnessAndReturnConfiguredFeatures(String messageFilePath, String testharnessPath)
			throws Exception {
		
		Runtime runtime = Runtime.getRuntime();
		runtime.exec(testharnessPath + "/eclipse.exe", null, new File(testharnessPath));

		// poll for the file and wait for it to appear
		File messageFile = new File(messageFilePath);

		waitReasonableTimeForProcessToWriteFileBeforeGivingUp(messageFile);

		List<String> configuredFeatures = null;
		configuredFeatures = readContents(messageFile);

		return configuredFeatures;
	}

	protected List<String> installFromFixturesReturnInstalledFeatures(String fixture, boolean removeDownloadDir,
			String messageFilenameAndPath, String messageFileSuffix, String testHarnessPath, File configFile)
			throws Exception {

		List<String> features;
		String messageFile = makeNewFileName(messageFilenameAndPath, messageFileSuffix);

		Map<String, String> addMe = new HashMap<String, String>();
		addMe.put(BundleUpdaterConfig.UPDATE_SITE_KEY, fixture);
		addMe.put(RCPEXAMPLE_MESSAGE_LOCATION_KEY, messageFile);
		
		List<String> removeMe = new ArrayList<String>();
		if (removeDownloadDir) {
			removeMe.add(BundleUpdaterConfig.DOWNLOAD_ROOT_KEY);
		}

		changeConfigurationini(addMe, removeMe, configFile);

		features = runTestHarnessAndReturnConfiguredFeatures(messageFile, testHarnessPath);
		return features;
	}

	protected void waitReasonableTimeForProcessToWriteFileBeforeGivingUp(File messageFile) throws InterruptedException {
		Long timeout = computeTimeout();

		while (!messageFile.exists()) {
			if (System.currentTimeMillis() > timeout) {
				fail("Timeout waiting for messagefile: [" + messageFile + "]");
			}
			Thread.sleep(100);
		}
	}

	protected void waitForLogMessageBeforeGivingUp(File logFile, String message) throws Exception {
		Long timeout = computeTimeout();

		while (!fileContainsString(logFile, message)) {
			if (System.currentTimeMillis() > timeout) {
				fail("Timeout waiting for message to appear in file: [" + logFile + "], [" + message + "]");
			}
			Thread.sleep(100);
		}
	}

	protected int countOccurancesOfListItemsStartingWithPrefix(List<String> list, String prefix) {
		int count = 0;
		for (String string : list) {
			if (string.startsWith(prefix))
				count++;
		}
		return count;
	}

	protected List<String> readContents(File messageFile) throws FileNotFoundException, IOException {

		List<String> lines = new ArrayList<String>();

		// pull out list of configured features
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(messageFile));
			String line = br.readLine();
			while (line != null) {
				lines.add(line);
				line = br.readLine();
			}
		} finally {
			try {
				br.close();
			} catch (Exception ex) {
			}
		}

		return lines;

	}

	protected void changeConfigurationini(Map<String, String> addMe, List<String> removeMe, File configini)
			throws Exception {

		FileInputStream in = null;
		FileOutputStream out = null;

		in = new FileInputStream(configini);

		Properties config = new Properties();
		config.load(in);
		in.close();

		config.putAll(addMe);

		for (String key : removeMe) {
			config.remove(key);
		}

		out = new FileOutputStream(configini, false);
		config.store(out, null);
		out.close();
	}

	protected String makeNewFileName(String filenameAndPath, String filenameSuffix) {
		return filenameAndPath + System.currentTimeMillis() + filenameSuffix;
	}

	protected void addProvisioningToConfig(File iniFile, String provisioningFilename) throws Exception {
		HashMap<String, String> addMeToConfig = new HashMap<String, String>();
		addMeToConfig.put(BundleUpdaterConfig.PROVISIONING_LOCATION_KEY, "file:///" + provisioningFilename);
		List<String> removeMe = new ArrayList<String>();
		changeConfigurationini(addMeToConfig, removeMe, iniFile);
	}

	protected void writeProvisioningInfoToNewFile(String[] lines, String provisioningFilename)
			throws FileNotFoundException {
		
		File provFile = new File(provisioningFilename);
		PrintWriter printWriter = new PrintWriter(provFile);
		for (String string : lines) {
			printWriter.write(string + "\r\n");
		}
		printWriter.flush();
		printWriter.close();
	}

	protected void copyFile(File source, File dest) throws IOException {
		if (!dest.exists()) {
			dest.createNewFile();
		}
		FileChannel sourceChannel = new FileInputStream(source).getChannel();
		FileChannel destChannel = new FileOutputStream(dest).getChannel();
	
		destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	
		sourceChannel.close();
		destChannel.close();
	}

	protected List<String> installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(String fixture, String featureSuffix,
			boolean removeDownloadDir, String messageFilenameAndPath, String messageFileSuffix, String productExeDirectory, 
			File configIniFile, String featureName) throws Exception {

			List<String> features = installFromFixturesReturnInstalledFeatures(fixture, removeDownloadDir,
					messageFilenameAndPath, messageFileSuffix, productExeDirectory, configIniFile);

			assertEquals("Features are: [" + features + "]", 1, countOccurancesOfListItemsStartingWithPrefix(features,
					featureName + featureSuffix));

			return features;
		}
		
	protected List<String> installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(String fixture, String featureSuffix,
		boolean removeDownloadDir, String messageFilenameAndPath, String messageFileSuffix, String productExeDirectory, 
		File configIniFile) throws Exception {

		return installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(fixture, featureSuffix, removeDownloadDir, messageFilenameAndPath, messageFileSuffix, productExeDirectory, configIniFile, "org.eclipse.e4.enterprise.installer.test.fixture.feature");
	}
	
	private boolean fileContainsString(File logFile, String message) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(logFile));
		String line = reader.readLine();
		while (line != null) {
			if (line.contains(message))
				return true;
			line = reader.readLine();
		}
		reader.close();
		return false;
	}

	private void recreateDir(File directory) throws Exception {
		if (directory.exists()) {
			long timeout = computeTimeout();
			while (!recursiveDelete(directory)) {
				if (System.currentTimeMillis() > timeout) {
					fail("Unable to delete directory: " + directory);
				}
				Thread.sleep(100);
			}
		}
		directory.mkdirs();
	}

	private Long computeTimeout() {
		long now = System.currentTimeMillis();
		Long timeout = now + (40 * 1000);
		return timeout;
	}
	
	public void testToPreventFailuresForNoTestsInTheClass()
	{		
	}
}
