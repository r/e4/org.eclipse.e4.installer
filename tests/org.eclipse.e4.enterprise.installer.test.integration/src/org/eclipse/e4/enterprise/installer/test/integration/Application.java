/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.metaconfig.Configuration;
import org.eclipse.e4.enterprise.installer.BundleUpdater;
import org.eclipse.e4.enterprise.installer.InstallError;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;


/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {
	public static final String MESSAGE_LOCATION_KEY = Activator.PLUGIN_ID+".MESSAGE_LOCATION";
	private static final String FEATURE_PREFIX_ECLIPSE = "org.eclipse";
	
	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {

		Configuration configuration = Activator.getDefault().getConfiguration();
		String messageLocationString = (String) configuration.getProperties().get(MESSAGE_LOCATION_KEY);
		File messageLocationFile = new File(messageLocationString);
		
		messageLocationFile.getParentFile().mkdirs();
		messageLocationFile.delete();
		
		
		BundleUpdater installer = new BundleUpdater();

		boolean restart = false;
		try
		{
			restart = installer.update();
		}
		catch (InstallError e) {
			// boom - something's gone wrong. 
			// If this were a real app, we'd want to do something here. 
			// Like log it or tell user or generally be unhappy.
			//
			// currently, we will "exit ok" (restart == false) , as opposed to asking for a restart.
			// ... which for the purposes of this test, is fine.
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, "BOOM!  Something threw an exception!"));
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
		}
		
		if (restart) {
			System.out.println("Restarting");
			return IApplication.EXIT_RESTART;
		}
		
		//things are installed, any restarts have happened so now log which features are installed
		logInstalledFeatures(messageLocationFile,installer);
		return IApplication.EXIT_OK;
	}

	private void logInstalledFeatures(File messageLocationFile, BundleUpdater installer)
			throws IOException, InstallError {
		
		List<String> installedFeatures = installer.getInstalledFeatures();
		StringBuffer installedFeatureStrings = new StringBuffer("");
		
		FileWriter writer = new FileWriter(messageLocationFile,false);
		for (String feature : installedFeatures) {
			if (feature.startsWith(FEATURE_PREFIX_ECLIPSE)) {
				installedFeatureStrings.append(feature + "\r\n");
			}
		}
		
		writer.write(installedFeatureStrings.toString());
		writer.flush();
		writer.close();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
