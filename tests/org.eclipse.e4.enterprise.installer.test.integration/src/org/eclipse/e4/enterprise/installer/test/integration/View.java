/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

import org.eclipse.e4.core.metaconfig.Configuration;
import org.eclipse.e4.enterprise.installer.BundleUpdater;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.part.ViewPart;


public class View extends ViewPart {
	public static final String ID = "org.eclipse.e4.enterprise.installer.test.integration.view";

	private static final String FEATURE_PREFIX_ECLIPSE = "org.eclipse";

	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout());

		// Layout the UI: Installed features list
		new Label(parent, SWT.NULL).setText("Installed features");
		final List installedFeaturesUI = new List(parent, SWT.BORDER | SWT.V_SCROLL);

		Configuration configuration = Activator.getDefault().getConfiguration();

		String messageLocation = (String) configuration.getProperties().get(Application.MESSAGE_LOCATION_KEY);

		installedFeaturesUI.add("writing messages to:" + messageLocation);
		installedFeaturesUI.add("Installed features with \"" + FEATURE_PREFIX_ECLIPSE + "\" prefix:");

// FIXME: Remove Try/Catch later
//		try {
			BundleUpdater bundleUpdater = new BundleUpdater();
			for (String feature : bundleUpdater.getInstalledFeatures()) {
				if (feature.startsWith(FEATURE_PREFIX_ECLIPSE)) {
					installedFeaturesUI.add(feature);
				}
			}
//		} catch (InstallError e) {
//			// TODO Auto-generated catch block
//			installedFeaturesUI.add("Nasty Exception");
//			installedFeaturesUI.add(e.getMessage());
//		}

		installedFeaturesUI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
	}
}