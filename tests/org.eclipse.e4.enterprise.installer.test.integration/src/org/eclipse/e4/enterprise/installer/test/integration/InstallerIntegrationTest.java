/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.e4.ui.test.utils.FixturesLocation;

public class InstallerIntegrationTest extends IntegrationTestCase {

	// This is the eclipse app that the tests will use
	private static final String PRODUCT_DIR = "C:/temp/exported_installer_integration_test_product";
	private static final String PRODUCT_EXE_DIR = PRODUCT_DIR + "/eclipse";
	
	protected static final String SITE400_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.0/site.xml";
	protected static final String SITE401_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_4.0.1/site.xml";
	protected static final String SITE500_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/site_5.0.0/site.xml";
	
	private static final String SMALL1_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/org.eclipse.e4.enterprise.installer.test.performance.fixture.small1.updatesite/site.xml";
	private static final String SMALL2_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/org.eclipse.e4.enterprise.installer.test.performance.fixture.small2.updatesite/site.xml";
	private static final String SMALL3_FIXTURE = FixturesLocation.FIXTURE_ROOT + "/org.eclipse.e4.enterprise.installer.test.performance.fixture.small3.updatesite/site.xml";

	private static final String DOWNLOAD_DIR = PRODUCT_EXE_DIR + "/iit";
	private static final String DOWNLOAD_ROOT_DIR = DOWNLOAD_DIR + "/root";
	private static String MESSAGE_FILENAME_AND_PATH = DOWNLOAD_DIR + "/message-file"; 

	private static final String MESSAGE_FILENAME_SUFFIX = ".txt";

	private static final String CONFIGURATION_FILENAME = "configuration.ini";

	private File backupOriginalIni;
	private File configini = new File(PRODUCT_EXE_DIR + "/" + CONFIGURATION_FILENAME);

	public InstallerIntegrationTest() throws IOException {
		// back up the ini file so it can be restored during setup.
		// why? Because the tests fiddle with it.
		backupOriginalIni = File.createTempFile("orginial-config", "ini");
		copyFile(configini, backupOriginalIni);

		System.out.println("back up is here: [" + backupOriginalIni.getAbsolutePath() + "]");
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		copyFile(backupOriginalIni, configini);
	}

	@Override
	protected void tearDown() throws Exception {
		super.setUp();
		copyFile(backupOriginalIni, configini); 
	}
	
	public void testUnjarredFeaturesAreProperlyCopied() throws Exception {

		// this test relies on other tests to verify that local copies are used
		// "testLocalCopyIsBeingUsed()", see InstallerProvisioningIntegrationTest.java.
		//
		// This test is verifying that a feature that demands to have its JARs
		// expanded is treated correctly in the whole "copy from local area"
		// process.

		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// install feature that will be expanded on install
		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture";
		List<String> features = installFromFixturesReturnInstalledFeatures(SITE_FIXTURE_EXPANDED_JAR, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, FIXTURE_EXPANDED_JAR_FEATURE));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix));

		// install additional feature and verify that expanded feature is copied
		String fixtureList = SITE_FIXTURE_EXPANDED_JAR + "  " + SITE401_FIXTURE;
		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(fixtureList, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, FIXTURE_EXPANDED_JAR_FEATURE));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures,
				"org.eclipse.e4.enterprise.installer.test.fixture.feature4.0.1"));
		assertEquals(2, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix));
	}

	public void testManyVersionsOfSameFeature() throws Exception {
		String featureID = "org.eclipse.e4.enterprise.installer.test.fixture.feature";

		String fixtureList = SITE400_FIXTURE + " " + SITE401_FIXTURE + " " + SITE500_FIXTURE;
		List<String> features = installFromFixturesReturnInstalledFeatures(fixtureList, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		assertEquals(3, countOccurancesOfListItemsStartingWithPrefix(features, featureID));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "4.0.0"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "4.0.1"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featureID + "5.0.0"));
	}

	public void testInstallsFromMultipleUpdateSitesOK_AndRemovalOnSubsequentUpdate() throws Exception {
		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.performance.fixture.small";

		String fixtureList = SMALL1_FIXTURE + " " + SMALL2_FIXTURE + " " + SMALL3_FIXTURE + " ";
		List<String> features = installFromFixturesReturnInstalledFeatures(fixtureList, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "1.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "2.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(features, featurePrefix + "3.feature"));

		List<String> newFeatures = installFromFixturesReturnInstalledFeatures(SMALL2_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "1.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "2.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(newFeatures, featurePrefix + "3.feature"));

	}

	// public void testNoExplicitSettingOfDownLoadRoot_allStillWorks() throws
	// Exception {
	// installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(SITE400_FIXTURE,
	// "4.0.0", true);
	// installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(SITE401_FIXTURE,
	// "4.0.1", true);
	// installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(SITE500_FIXTURE,
	// "5.0.0", true);
	// installFromUpdateSiteFixtureAndCheckFeatureInstalledOK(SITE401_FIXTURE,
	// "4.0.1", true);
	// }

	public void testInstall400_UpdateTo401_UpdateTo500_RollbackTo401() throws Exception {

		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE400_FIXTURE, "4.0.0", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE500_FIXTURE, "5.0.0", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
	}

	public void testSiteWithMultipleFeatures_worksOK() throws Exception {
		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";

		List<String> featuresABC = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "c.feature"));

		List<String> featuresDEF = installFromFixturesReturnInstalledFeatures(SITE_DEF_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "d.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "e.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "f.feature"));

		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "a.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "b.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresDEF, featurePrefix + "c.feature"));

	}

	public void testCorruptedDownload_rollsBackToInitialState() throws Exception {
		String featurePrefix = "org.eclipse.e4.enterprise.installer.test.fixture.many_";

		// 1. reset to know state: install from a site with multiple features (A
		// B and C)
		List<String> featuresABC = installFromFixturesReturnInstalledFeatures(SITE_ABC_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresABC, featurePrefix + "c.feature"));

		// 2. attempt to install from a site with some good, some bad
		// (corrupted)
		// features, say D, E and F where E is cuffed in some way.
		// test that we roll back to initial state.

		List<String> featuresRolledBack = installFromFixturesReturnInstalledFeatures(SITE_DEF_CORRUPTED_FIXTURE, false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "a.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "b.feature"));
		assertEquals(1, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "c.feature"));

		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "d.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "e.feature"));
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(featuresRolledBack, featurePrefix + "f.feature"));

	}

	public void testInstallFromNonexistantUpdateSite_rollsBackToInitialState() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// find the current download site so that we can make sure it is still
		// there and the current download site later on
		File downloadRootDirFile = new File(DOWNLOAD_ROOT_DIR);
		String[] dirs = downloadRootDirFile.list();
		assertEquals(1, dirs.length);
		String expectedCurrentSiteDirname = dirs[0];

		// try and install 5.0.0 but from an update site that doesn't exist
		// assert that 4.0.1 is still installed
		List<String> features = installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE509_NONEXISTANT_FIXTURE,
				"4.0.1", false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		// ... and 5.0.0 isn't
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features,
				"org.eclipse.e4.enterprise.installer.test.fixture.feature5.0.0"));

		// assert that the only directory is still the current directory
		dirs = downloadRootDirFile.list();
		assertEquals(1, dirs.length);
		assertEquals(expectedCurrentSiteDirname, dirs[0]);
	}

	public void testInstallFromUpdateSiteThatIsMissingPlugins_rollsBackToInitialState() throws Exception {
		// reset the state to S(0)
		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE401_FIXTURE, "4.0.1", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		// find the current download site so that we can make sure it is still
		// there and the current download site later on
		File downloadRootDirFile = new File(DOWNLOAD_ROOT_DIR);
		String[] dirs = downloadRootDirFile.list();
		assertEquals(1, dirs.length);
		String expectedCurrentSiteDirname = dirs[0];

		// try and install 5.0.0 but from a broken update site (the plugins are
		// missing)

		// assert that 4.0.1 is still installed
		List<String> features = installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE500_BROKEN_FIXTURE, "4.0.1",
				false, MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);
		// ... and 5.0.0 isn't
		assertEquals(0, countOccurancesOfListItemsStartingWithPrefix(features,
				"org.eclipse.e4.enterprise.installer.test.fixture.feature5.0.0"));

		// assert that the only directory is still the current directory
		dirs = downloadRootDirFile.list();

		/*
		 * 2009-08-28 currently, if we plugins are missing, the Site is still
		 * created on disc even though it is not in the eclipse's configured
		 * sites. i.e. detritus left on disc.
		 * 
		 * assertEquals(1, dirs.length); <--this is 2 not 1 using the current
		 * implementation
		 */

		assertEquals(expectedCurrentSiteDirname, dirs[0]);

		// the next time we do a valid upgrade, all the old detritus is cleaned
		// up.

		installFromUpdateSiteFixtureAndCheckFeatureInstalledOKUsingDefaultFeature(SITE500_FIXTURE, "5.0.0", false,
				MESSAGE_FILENAME_AND_PATH, MESSAGE_FILENAME_SUFFIX, PRODUCT_EXE_DIR, configini);

		dirs = downloadRootDirFile.list();
		assertEquals(1, dirs.length);

	}
}
