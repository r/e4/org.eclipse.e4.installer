/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package org.eclipse.e4.enterprise.installer.test.integration.fixture;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.enterprise.installer.BundleUpdater;
import org.eclipse.e4.enterprise.installer.InstallError;
import org.eclipse.ui.PlatformUI;


class BackgroundUpdater implements Runnable {
	private final Application application;
	private BundleUpdater updater;
	private String url;
	private int hours;
	
	public BackgroundUpdater(Application application, BundleUpdater updater, String url, int hours) {
		this.application = application;
		this.updater = updater;
		this.url = url;
		this.hours = hours;
	}
	
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000*60*60 * hours);  // update every hour
			} catch (InterruptedException e) {
				// Application is closing; return
				return;
			}
			Job updateJob = new Job("Updating application") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						monitor.beginTask("Updating application", 1);
						if (updater.update(url)) {
							PlatformUI.getWorkbench().restart();
						}
						monitor.worked(1);
					} catch (InstallError e) {
						BackgroundUpdater.this.application.logError(e.getMessage(), e);
					}
					return null;
				}
			};
			updateJob.schedule();
		}
	}
}

