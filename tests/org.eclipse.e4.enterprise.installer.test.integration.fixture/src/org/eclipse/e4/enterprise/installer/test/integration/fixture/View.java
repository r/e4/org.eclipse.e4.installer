/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration.fixture;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.configuration.IInstallConfiguration;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.VersionedIdentifier;

public class View extends ViewPart {
	public static final String ID = "org.eclipse.e4.enterprise.installer.test.integration.fixture.view";

	private IConfiguredSite[] sites;

	private List installedFeatures;

	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout());

		// Layout the UI: Installed features list
		new Label(parent, SWT.NULL).setText("Installed features");
		installedFeatures = new List(parent, SWT.BORDER|SWT.V_SCROLL);
		installedFeatures.add("Computing initial list...");
		installedFeatures.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		// Populate the installed features list and hook up the events
		// This is demo code so we willingly mix presentation and behavior :-)

		/*
		 * NOTE: This is demo code so we create a Thread directly.  Production code should
		 * use the Eclipse Jobs API.
		 */
		Thread backgroundInit = new Thread(new Runnable() {
			public void run() {
				try {
					IInstallConfiguration config = SiteManager.getLocalSite().getCurrentConfiguration();
					sites = config.getConfiguredSites();
					populateInstalledFeaturesList(installedFeatures);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
			
			private void populateInstalledFeaturesList(final List installedFeatures) {
				// NOTE: Production code should use Eclipse's Jobs API to get on the Display thread...
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						installedFeatures.removeAll();
					}
				});
				try {
					for (IConfiguredSite configuredSite : sites) {
						IFeatureReference[] featureReferences = configuredSite.getConfiguredFeatures();
						for (IFeatureReference featureReference : featureReferences) {
							VersionedIdentifier versionedIdentifier = featureReference.getVersionedIdentifier();
							final String item = versionedIdentifier.getIdentifier() + versionedIdentifier.getVersion();
							// NOTE: Production code should use Eclipse's Jobs API to get on the Display thread...
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									installedFeatures.add(item);
								}
							});
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		});
		backgroundInit.start();
	}

	public void setFocus() {
		installedFeatures.setFocus();
	}
}
