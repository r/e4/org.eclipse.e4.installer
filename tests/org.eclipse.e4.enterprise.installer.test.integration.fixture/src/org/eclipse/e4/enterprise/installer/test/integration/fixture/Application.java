/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.enterprise.installer.test.integration.fixture;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.metaconfig.Configuration;
import org.eclipse.e4.core.metaconfig.ConfigurationException;
import org.eclipse.e4.enterprise.installer.InstallError;
import org.eclipse.e4.enterprise.installer.ui.swt.BundleUpdaterHelper;
import org.eclipse.e4.enterprise.installer.ui.swt.BundleUpdaterHelper.UpdateResult;
import org.eclipse.e4.enterprise.installer.ui.swt.BundleUpdaterHelper.UpdateStatus;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.configuration.IInstallConfiguration;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.VersionedIdentifier;


/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {
	public static final String MESSAGE_LOCATION_KEY = "org.eclipse.e4.enterprise.installer.test.integration.MESSAGE_LOCATION";
	
	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		Activator activator = Activator.getDefault();
		
		activator.getLog().log(new Status(Status.INFO, "!STARTUP", "Startup at " + new Date()));
		
		Configuration configuration = activator.getConfiguration();
		String messageLocationString = (String) configuration.getProperties().get(MESSAGE_LOCATION_KEY);
		if (messageLocationString == null) {
			messageLocationString = "messages.txt";
		}
		File messageLocationFile = new File(messageLocationString);
		try {
			UpdateStatus status = configureInstaller(configuration);
			UpdateResult result = status.result;
			switch (result) {
			case FAILURE_INSTALL_ERROR:
				logError(status.failureException.getMessage(), status.failureException);
				return IApplication.EXIT_OK;
			case FAILURE_USER_CANCELLED:
			case SUCCESS_RESTART_NOT_REQUIRED:
				//things are installed, any restarts have happened so now log which features are installed
				logInstalledFeatures(messageLocationFile);
				activator.getLog().log(new Status(Status.INFO, "!STARTUP", "No updates downloaded, continuing..."));
				return IApplication.EXIT_OK;
			case SUCCESS_RESTART_REQUIRED:
				activator.getLog().log(new Status(Status.INFO, "!STARTUP", "Downloaded updates; Restarting..."));
				return IApplication.EXIT_RESTART;
			default:
				logError("Should not get to this case!", new RuntimeException());
				assert(false);
			}
		} catch (ConfigurationException e) {
			// boom - something's gone wrong. 
			// If this were a real app, we'd want to do something here. 
			// Like log it or tell user or generally be unhappy.
			//
			// currently, we will "exit ok" (restart == false) , as opposed to asking for a restart.
			// ... which for the purposes of this test, is fine.
			String message = "Unable to access configuration";
			logError(message, e);
			return IApplication.EXIT_OK;
		} catch (InstallError e) {
			logError(e.getMessage(), e);
			return IApplication.EXIT_OK;
		} catch (IOException e) {
			logError(e.getMessage(), e);
			e.printStackTrace();
		}
		
		return IApplication.EXIT_OK;
	}

	private static final String FEATURE_PREFIX_ECLIPSE = "org.eclipse";

	private void logInstalledFeatures(File messageLocationFile) throws IOException, InstallError {

		List<String> installedFeatures = getInstalledFeatures();
		StringBuffer installedFeatureStrings = new StringBuffer("");

		FileWriter writer = new FileWriter(messageLocationFile, false);
		for (String feature : installedFeatures) {
			if (feature.startsWith(FEATURE_PREFIX_ECLIPSE)) {
				installedFeatureStrings.append(feature + "\r\n");
			}
		}

		writer.write(installedFeatureStrings.toString());
		writer.flush();
		writer.close();
	}
	
	public List<String> getInstalledFeatures() {
		List<String> installedFeatures = new ArrayList<String>();
		try {
			IInstallConfiguration config = SiteManager.getLocalSite().getCurrentConfiguration();
			IConfiguredSite[] sites = config.getConfiguredSites();
			for (IConfiguredSite configuredSite : sites) {
				IFeatureReference[] featureReferences = configuredSite.getConfiguredFeatures();
				for (IFeatureReference featureReference : featureReferences) {
					VersionedIdentifier versionedIdentifier = featureReference.getVersionedIdentifier();
					String item = versionedIdentifier.getIdentifier() + versionedIdentifier.getVersion();
					installedFeatures.add(item);
				}
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return installedFeatures;
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
		updateThread.interrupt();
	}
	
	// Install updates, if available ----------------------------------------------------------------------
	
	private Thread updateThread;

	private UpdateStatus configureInstaller(Configuration config) throws ConfigurationException, InstallError {
		return new BundleUpdaterHelper().updateWithProgressDialog();
	}

	// For our example, we'll use the Eclipse logger, but you can use any logger you want.
	void logError(String message, Throwable e) {
		IStatus error = new Status(Status.ERROR, Activator.PLUGIN_ID, message, e);
		Activator.getDefault().getLog().log(error);
	}

}
